-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 26, 2015 at 10:01 AM
-- Server version: 5.5.41
-- PHP Version: 5.5.26-1+deb.sury.org~precise+1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `holiday`
--

-- --------------------------------------------------------

--
-- Table structure for table `hl_articles`
--

CREATE TABLE IF NOT EXISTS `hl_articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `author_id` int(11) NOT NULL,
  `time_created` datetime NOT NULL,
  `time_updated` datetime NOT NULL,
  `time_published` datetime NOT NULL,
  `content` longtext NOT NULL,
  `status` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `hl_articles`
--

INSERT INTO `hl_articles` (`id`, `name`, `title`, `author_id`, `time_created`, `time_updated`, `time_published`, `content`, `status`) VALUES
(1, 'what-is-lorem-ipsum', 'What Is Lorem Ipsum', 1, '2015-06-30 00:00:00', '2015-06-30 00:00:00', '2015-06-30 00:00:00', '<p>Lorem Ipsum\r\n is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 1),
(2, 'where-does-it-come-from', 'Where Does It Come From', 1, '2015-06-30 00:00:00', '2015-06-30 00:00:00', '2015-06-30 00:00:00', '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>\r\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 1),
(3, 'why-do-we-use-it', 'Why Do We Use It', 1, '2015-07-01 00:00:00', '2015-07-01 00:00:00', '2015-07-01 00:00:00', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>', 1),
(4, 'where-i-can-get-some', 'Where I Can Get Some', 1, '2015-07-02 00:00:00', '2015-07-02 00:00:00', '2015-07-02 00:00:00', '<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>', 1),
(5, 'lorem-ipsum', 'Lorem Ipsum', 1, '2015-07-03 00:00:00', '2015-07-03 00:00:00', '2015-07-03 00:00:00', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget faucibus tortor, et ultrices eros. Nam bibendum euismod tincidunt. Vivamus ullamcorper eleifend ligula a placerat. Pellentesque neque magna, vulputate non diam eget, condimentum consectetur arcu. Donec sit amet ante quis leo blandit molestie quis at enim. In hendrerit accumsan mi sed ultrices. Nam volutpat suscipit orci, eget consectetur libero viverra in. Suspendisse porttitor libero urna, sed fermentum metus tempor in. In porta nulla nec ex cursus, sit amet placerat dui euismod. Pellentesque gravida interdum ante. Fusce vestibulum leo a dolor condimentum placerat. Nullam sagittis dolor tellus, fermentum tempor enim facilisis a. Quisque non nisi in magna elementum vehicula. Donec orci diam, dictum et placerat id, iaculis molestie ipsum. Aliquam odio velit, tristique posuere ex eget, congue maximus leo. Sed tristique, risus sed facilisis fringilla, felis erat euismod lacus, at fermentum metus erat nec est.</p>', 0);

-- --------------------------------------------------------

--
-- Table structure for table `hl_categories`
--

CREATE TABLE IF NOT EXISTS `hl_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `title` varchar(256) NOT NULL,
  `author_id` int(11) NOT NULL,
  `time_created` datetime NOT NULL,
  `time_updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `hl_categories`
--

INSERT INTO `hl_categories` (`id`, `name`, `title`, `author_id`, `time_created`, `time_updated`) VALUES
(1, 'lorem-ipsum', 'Lorem Ipsum', 1, '2015-07-04 06:18:21', '2015-07-04 06:21:32');

-- --------------------------------------------------------

--
-- Table structure for table `hl_category_article`
--

CREATE TABLE IF NOT EXISTS `hl_category_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `hl_category_article`
--

INSERT INTO `hl_category_article` (`id`, `category_id`, `article_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `hl_comments`
--

CREATE TABLE IF NOT EXISTS `hl_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `fullname` varchar(256) NOT NULL,
  `time_created` datetime NOT NULL,
  `content` longtext NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `article_id` (`article_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `hl_comments`
--

INSERT INTO `hl_comments` (`id`, `article_id`, `fullname`, `time_created`, `content`, `status`) VALUES
(1, 1, 'Bruce Wayne', '2015-07-06 05:19:25', 'Some comment, not important, just ignore this comment. Bye', 2),
(2, 2, 'Bruce Wayne', '2015-07-06 05:25:25', 'Lorem ipsum dos color sit amet bla bla bla', 2),
(4, 5, 'asd', '2015-10-18 02:29:19', 'asd&#60;&#62;?&#60;', 2),
(5, 5, 'asd', '2015-10-17 23:31:59', 'asd&#60;&#62;?&#60;', 2),
(6, 5, 'Comment', '0000-00-00 00:00:00', 'The real Comment', 2),
(7, 5, 'Jauhar Arifin', '2015-10-18 00:01:15', 'Howdy Y&#39;all', 2),
(8, 5, 'Jauhar Arifin', '2015-10-18 00:04:06', 'Jauhar Arifin dos color sit amet', 2),
(9, 5, 'Jauhar Arifin', '2015-10-18 02:46:27', 'Komen Aneh', 2),
(10, 5, 'ASD', '2015-10-18 02:46:35', 'jauhar arifin', 2),
(11, 5, 'Reply', '2015-10-18 02:46:46', 'Inovasi Kincir Angin untuk PLTA (Pembangit Listrik Tenaga Angin)', 2),
(12, 5, 'Random Person', '2015-10-18 02:47:03', ' Scoot, the San Francisco-based electric scooter ride share network, has teamed up with Nissan to create a four-wheeled', 2),
(13, 5, 'Lorem Ipsum', '2015-10-18 02:47:18', 'Sed tristique, risus sed facilisis fringilla, felis erat euismod lacus, at fermentum metus erat nec est.', 2),
(14, 5, 'Viva La Vida', '2015-10-18 02:47:34', ' Vivamus ullamcorper eleifend ligula a placerat.', 2),
(15, 2, 'Jauhar Arifin', '2015-12-25 06:25:58', 'Tes komen', 2),
(16, 2, 'User Example', '2015-12-25 07:51:54', 'Comment example 2', 2),
(17, 2, 'User Example', '2015-12-25 07:52:23', 'Coba komen 3', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hl_config`
--

CREATE TABLE IF NOT EXISTS `hl_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(256) NOT NULL,
  `type` varchar(256) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `hl_config`
--

INSERT INTO `hl_config` (`id`, `key`, `type`, `value`) VALUES
(1, 'siteName', 'string', 'Holiday Framework'),
(3, 'view.theme', 'string', 'developer'),
(4, 'article.articles_per_page', 'int', '3'),
(5, 'comment.comments_per_page', 'int', '10'),
(6, 'comment.default_status', 'int', '1');

-- --------------------------------------------------------

--
-- Table structure for table `hl_users`
--

CREATE TABLE IF NOT EXISTS `hl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `time_created` datetime NOT NULL,
  `time_updated` datetime NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `nickname` varchar(255) NOT NULL,
  `date_birth` date NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `data` text NOT NULL,
  `time_lastvisit` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `hl_users`
--

INSERT INTO `hl_users` (`id`, `username`, `email`, `password`, `salt`, `time_created`, `time_updated`, `fullname`, `nickname`, `date_birth`, `gender`, `data`, `time_lastvisit`) VALUES
(1, 'jauhararifin', 'jauhararifin10@gmail.com', 'yyvd5hJhtnbe6', 'yyAieOJBA3d9Pb0z', '2015-07-02 13:36:00', '2015-07-02 13:36:00', 'Jauhar Arifin', 'Jauhar', '1997-06-18', 1, '', '2015-07-02 13:36:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
