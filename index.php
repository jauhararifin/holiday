<?php

define ('BASEPATH', __DIR__.'/');
define ('SYSTEM_PATH', BASEPATH.'web/');

define ('HL_DEBUG', TRUE);
define ('HL_ENVIRONMENT', 'development');

if (HL_ENVIRONMENT == 'release')
{
	error_reporting(0);
}

ini_set('magic_quotes_runtime', 0);

function __autoload(string $class)
{
	$files = array(
		str_replace('\\', '/', SYSTEM_PATH.$class.'.php'),
		str_replace('\\', '/', SYSTEM_PATH.'libs/'.$class.'.php'),
	);

	foreach ($files as $file)
		if (file_exists($file))
		{
			require_once $file;
			return;
		}
}

Holiday::run(SYSTEM_PATH.'config/Config.php');
