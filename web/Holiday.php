<?php

class Holiday {

	public static $app;

	public static function run(string $configFile)
	{
		if (self::$app == NULL)
			self::$app = new \core\Application($configFile);
		self::$app->run();
	}

	public static function setHeaderStatus(int $code=200, string $text='')
	{
		$stati = array(
			200	=> 'OK',
			201	=> 'Created',
			202	=> 'Accepted',
			203	=> 'Non-Authoritative Information',
			204	=> 'No Content',
			205	=> 'Reset Content',
			206	=> 'Partial Content',

			300	=> 'Multiple Choices',
			301	=> 'Moved Permanently',
			302	=> 'Found',
			304	=> 'Not Modified',
			305	=> 'Use Proxy',
			307	=> 'Temporary Redirect',

			400	=> 'Bad Request',
			401	=> 'Unauthorized',
			403	=> 'Forbidden',
			404	=> 'Not Found',
			405	=> 'Method Not Allowed',
			406	=> 'Not Acceptable',
			407	=> 'Proxy Authentication Required',
			408	=> 'Request Timeout',
			409	=> 'Conflict',
			410	=> 'Gone',
			411	=> 'Length Required',
			412	=> 'Precondition Failed',
			413	=> 'Request Entity Too Large',
			414	=> 'Request-URI Too Long',
			415	=> 'Unsupported Media Type',
			416	=> 'Requested Range Not Satisfiable',
			417	=> 'Expectation Failed',

			500	=> 'Internal Server Error',
			501	=> 'Not Implemented',
			502	=> 'Bad Gateway',
			503	=> 'Service Unavailable',
			504	=> 'Gateway Timeout',
			505	=> 'HTTP Version Not Supported'
		);

		if ($code < 1)
			$code = 500;
		if ($text = '' && array_key_exists($code, $stati))
			$text = $stati[$code];

		$server_protocol = $_SERVER['SERVER_PROTOCOL'] ?? 'HTTP/1.1';

		if (substr(php_sapi_name(), 0, 3) == 'cgi')
			header('Status: '.$code.' '.$text, TRUE);
		else
			header($server_protocol.' '.$code.' '.$text, TRUE, $code);
	}

	public static function showError(string $message, int $statusCode = 500, string $template = 'error', string $title='Something Error', bool $exit = FALSE)
	{
		$controller = NULL;
		if (file_exists(SYSTEM_PATH.'app/controllers/ErrorController.php'))
			$controller = new \app\controllers\ErrorController($message, $statusCode, $template, $title, $exit);
		else
			$controller = new \core\controller\ErrorController($message, $statusCode, $template, $title, $exit);

		$controller->actionShow();

		if ($exit) exit;
	}

	public function controller() : \core\Controller
	{
		return self::$app->controller;
	}

}
