<?php

namespace app\controllers\administrator;

class ArticleController extends \app\core\AdminController {

	private $articleModel = NULL;

	public function __construct()
	{
		parent::__construct();
		$this->articleModel = new \app\models\ArticleModel;
		\helpers\Loader::load('ajax');
	}

	public function actionIndex()
	{
		$totalArticles = intval($this->articleModel->count() ?? 0);
		$articlesPerPage = intval($_GET['articlesPerPage'] ?? 10);
		$totalPage = intval(($totalArticles + $articlesPerPage - 1) / $articlesPerPage);
		$currentPage = intval($_GET['currentPage'] ?? 1);

		if ($currentPage < 1)
			$currentPage = 1;

		$articles = $this->articleModel->findByCriteria(
			[],
			$articlesPerPage,
			$articlesPerPage * ($currentPage - 1),
			['field'=>['time_updated','id'],'order'=>'DESC']
		);

		$data = [
			'articles' => $articles,
			'totalArticles'	=> $totalArticles,
			'articlesPerPage' => $articlesPerPage,
			'totalPage' => $totalPage,
			'currentPage' => $currentPage,
		];

		if (isAjaxRequest())
		{
			$data['articles'] = [];
			foreach ($articles as $article)
				$data['articles'][] = $article->toArray();
			$this->view()->print(json_encode($data));
		}else
			$this->view()->render('articles',$data);
	}

	public function actionDelete()
	{
		if (isset($_POST['action']) && $_POST['action'] == 'delete')
		{
			$id = $_POST['id'] ?? 0;
			if ($this->articleModel->findUnique([['where','id','=',$id]])->delete('id'))
				$this->_messageStatus->addMessage(\messageStatus::MESSAGE_SUCCESS, "delete_article", lang('Admin','article_delete_success'));
			else
				$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "delete_article", lang('Admin','article_delete_error'));
		}
		return $this->actionIndex();
	}
	
	public function actionCreate()
	{
		$articleForm = new \app\models\administrator\ArticleForm;

		if (isset($_POST['action']) && $_POST['action'] == 'create')
		{
			$name = $_POST['name'] ?? '';
			$title = $_POST['title'] ?? '';
			$content = $_POST['content'] ?? '';
			$status = intval($_POST['status'] ?? 0);
			
			$articleForm->fillData([
				'name' => $name,
				'title' => $title,
				'content'=> $content,
				'status' => $status,
			]);

			if ($articleForm->runValidation())
			{
				$article = new \app\models\articleModel;
				if ($article->count('',['criteria'=>[['where','name','=',$name]]]) <= 0)
				{
					$article->name = $name;
					$article->title = $title;
					$article->author_id = $this->loggedUser->id;
					$article->content = $articleForm->getSanitizedData('content');
					$article->status = $status;
					$article->time_created = date("Y-m-d H:i:s");
					$article->time_updated = date("Y-m-d H:i:s");
					$article->time_published = ($status != 0) ? date("Y-m-d H:i:s") : ("0000-00-00 00:00:00");

					if ($id = $article->insert())
					{
						$this->_messageStatus->addFlashMessage(\messageStatus::MESSAGE_SUCCESS, "create_article", lang('Admin','article_create_success'));
						redirect(baseUrl().'administrator/article/edit/'.$id);
					}else
						$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "create_article", lang('Admin','article_create_error'));
				} else
					$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "create_article", lang('Admin','caegory_name_already_exists'));
			} else
				$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "create_article", lang('Admin','article_create_data_invalid'));
		}

		$this->view()->render('create_article',[
			'articleForm' => $articleForm,
		]);
	}

	public function actionEdit(int $id = 0)
	{
		if ($id <= 0)
			redirect(baseUrl().'administrator/article');

		$articleForm = new \app\models\administrator\ArticleForm;

		$article = $this->articleModel->findUnique([['where','id','=',$id]]);

		if (isset($_POST['action']) && $_POST['action'] == 'edit')
		{
			$title = $_POST['title'] ?? '';
			$content = $_POST['content'] ?? '';
			$status = intval($_POST['status'] ?? 0);
			
			$articleForm->fillData([
				'title' => $title,
				'content' => $content,
				'status' => $status,
			]);

			if ($articleForm->runValidation())
			{
				$article->title = $title;
				$article->content = $articleForm->getSanitizedData('content');
				$article->status = $status;
				$article->time_updated = date("Y-m-d H:i:s");
				$article->time_published = ($status != 0) ? date("Y-m-d H:i:s") : ("0000-00-00 00:00:00");

				if ($article->update('id',['title','time_updated','content','status','time_published']))
				{
					$this->_messageStatus->addFlashMessage(\messageStatus::MESSAGE_SUCCESS, "edit_article", lang('Admin','article_edit_success'));
					redirect(baseUrl().'administrator/article/edit/'.$id);
				}else
					$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "create_article", lang('Admin','article_edit_error'));
			} else
				$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "create_article", lang('Admin','article_edit_data_invalid'));
		}

		$this->view()->render('edit_article',[
			'articleForm' => $articleForm,
			'article' => $article,
		]);
	}

}
