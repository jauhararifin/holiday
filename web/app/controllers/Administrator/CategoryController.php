<?php

namespace app\controllers\administrator;

class CategoryController extends \app\core\AdminController {

	private $categoryModel = NULL;

	public function __construct()
	{
		parent::__construct();
		$this->categoryModel = new \app\models\CategoryModel;
		\helpers\Loader::load('ajax');
	}

	public function actionIndex()
	{		
		$totalCategories = intval($this->categoryModel->count() ?? 0);
		$categoriesPerPage = intval($_GET['categoriesPerPage'] ?? 10);
		$totalPage = intval(($totalCategories + $categoriesPerPage - 1) / $categoriesPerPage);
		$currentPage = intval($_GET['currentPage'] ?? 1);

		if ($currentPage < 1)
			$currentPage = 1;

		$categories = $this->categoryModel->findByCriteria(
			[],
			$categoriesPerPage,
			$categoriesPerPage * ($currentPage - 1),
			['field'=>['time_updated','id'],'order'=>'DESC']
		);

		$data = [
			'categories'		=> $categories,
			'totalCategories'	=> $totalCategories,
			'categoriesPerPage' => $categoriesPerPage,
			'totalPage'			=> $totalPage,
			'currentPage'		=> $currentPage,
		];

		if (isAjaxRequest())
		{
			$data['categories'] = [];
			foreach ($categories as $category)
				$data['categories'][] = $category->toArray();
			$this->view()->print(json_encode($data));
		}else
			$this->view()->render('categories',$data);
	}

	public function actionDelete()
	{
		if (isset($_POST['action']) && $_POST['action'] == 'delete')
		{
			$id = $_POST['id'] ?? 0;
			if ($this->categoryModel->findUnique([['where','id','=',$id]])->delete('id'))
				$this->_messageStatus->addMessage(\messageStatus::MESSAGE_SUCCESS, "delete_category", lang('Admin','category_delete_success'));
			else
				$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "delete_category", lang('Admin','category_delete_error'));
		}
		return $this->actionIndex();
	}

	public function actionCreate()
	{
		$categoryForm = new \app\models\administrator\CategoryForm;

		if (isset($_POST['action']) && $_POST['action'] == 'create')
		{
			$name = $_POST['name'] ?? '';
			$title = $_POST['title'] ?? '';
			$status = intval($_POST['status'] ?? 0);
			
			$categoryForm->fillData([
				'name' => $name,
				'title' => $title,
				'status' => $status,
			]);

			if ($categoryForm->runValidation())
			{
				$category = new \app\models\CategoryModel;
				if ($category->count('',['criteria'=>[['where','name','=',$name]]]) <= 0)
				{
					$category->name = $name;
					$category->title = $title;
					$category->author_id = $this->loggedUser->id;
					$category->time_created = date("Y-m-d H:i:s");
					$category->time_updated = date("Y-m-d H:i:s");
					$category->status = $status;

					if ($id = $category->insert())
					{
						$this->_messageStatus->addFlashMessage(\messageStatus::MESSAGE_SUCCESS, "create_category", lang('Admin','category_create_success'));
						redirect(baseUrl().'administrator/category/edit/'.$id);
					}else
						$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "create_category", lang('Admin','category_create_error'));
				} else
					$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "create_category", lang('Admin','caegory_name_already_exists'));
			} else
				$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "create_category", lang('Admin','category_create_data_invalid'));
		}

		$this->view()->render('create_category',[
			'categoryForm' => $categoryForm,
		]);
	}

	public function actionEdit(int $id = 0)
	{
		if ($id <= 0)
			redirect(baseUrl().'administrator/category');

		$categoryForm = new \app\models\administrator\CategoryForm;

		$category = $this->categoryModel->findUnique([['where','id','=',$id]]);

		if (isset($_POST['action']) && $_POST['action'] == 'edit')
		{
			$title = $_POST['title'] ?? '';
			$status = intval($_POST['status'] ?? 0);

			$categoryForm->fillData([
				'name' => $category->name ?? '',
				'title' => $title,
				'status' => $status,
			]);

			if ($categoryForm->runValidation())
			{
				$category->title = $title;
				$category->time_updated = date("Y-m-d H:i:s");
				$category->status = $status;

				if ($category->update('id',['title','time_updated','status']))
				{
					$this->_messageStatus->addFlashMessage(\messageStatus::MESSAGE_SUCCESS, "edit_category", lang('Admin','category_edit_success'));
					redirect(baseUrl().'administrator/category/edit/'.$id);
				}else
					$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "edit_category", lang('Admin','category_edit_error'));
			} else
				$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "edit_category", lang('Admin','category_edit_data_invalid'));
		} else if (isset($_POST['action']) && $_POST['action'] == 'addArticle')
		{
			$id = intval($_POST['id'] ?? 0);
			if ($category->addArticle($id))
			{
				$this->_messageStatus->addMessage(\messageStatus::MESSAGE_SUCCESS, "edit_category", lang('Admin','article_add_success'));
			} else
			{
				$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "edit_category", lang('Admin','article_add_error'));
			}
		} else if (isset($_POST['action']) && $_POST['action'] == 'removeArticle')
		{
			$id = intval($_POST['id'] ?? 0);
			if ($category->removeArticle($id))
			{
				$this->_messageStatus->addMessage(\messageStatus::MESSAGE_SUCCESS, "edit_category", lang('Admin','article_remove_success'));
			} else
			{
				$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "edit_category", lang('Admin','article_remove_error'));
			}
		}

		$this->view()->render('edit_category',[
			'categoryForm' => $categoryForm,
			'category' => $category,
		]);
	}

}
