<?php

namespace app\controllers\administrator;

class MainController extends \app\core\AdminController {

	public function __construct()
	{
		parent::__construct();
	}

	public function actionIndex()
	{
		$articleModel = new \app\models\ArticleModel;
		$categoryModel = new \app\models\CategoryModel;

		$articles = $articleModel->findByCriteria([],10,0,['field'=>'time_updated','order'=>'DESC']);
		$categories = $categoryModel->findByCriteria([],10,0,['field'=>'time_updated','order'=>'DESC']);
		
		$this->view()->render('main',['articles'=>$articles,'categories'=>$categories]);
	}


}
