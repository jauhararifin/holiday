<?php

namespace app\controllers\administrator;

class UserController extends \app\core\Controller {

	private $loggedUser = NULL;
	private $userModel = NULL;

	public function __construct()
	{
		parent::__construct();
		
		$this->view()->setConfig('theme', 'admin');

		\Holiday::$app->language->loadLanguage('Admin');

		$this->userModel = new \app\models\UserModel;

		$userid = intval(\Holiday::$app->session->get('loggedUser'));
		$this->loggedUser = $this->userModel->findUnique([['where','id','=',($userid ?? 0)]]);
	}

	public function actionLogin()
	{
		if (($this->loggedUser->id ?? 0) > 0)
		{
			redirect(baseUrl().'administrator');
			exit;
		}

		$loginForm = new \app\models\LoginForm;

		if (isset($_POST['action']) && $_POST['action'] === 'login')
		{
			$username = $_POST['username'] ?? '';
			$password = $_POST['password'] ?? '';
			$remember = isset($_POST['remember']);

			$loginForm->fillData(['username'=>$username,'password'=>$password]);

			if ($loginForm->runValidation())
			{
				$user = $this->userModel->findUnique([['where','username','=',$username]]);
				if ($user != NULL && password_verify($password, $user->password))
				{
					$userSession = [];
					foreach (['id','username','fullname','email','time_created','time_updated','nickname','date_birth','gender','time_lastvisit'] as $key)
						$userSession[$key] = $user->$key ?? '';
					
					\Holiday::$app->session->set('loggedUser', $userSession['id'] ?? 0);
					
					$this->_messageStatus->addFlashMessage(\messageStatus::MESSAGE_SUCCESS, "login", lang('Admin','login_success'));
					redirect(baseUrl().'administrator');
				} else
					$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "login", lang('Admin','wrong_username_password'));
			} else
				$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "login", lang('Admin','login_data_invalid'));
		}

		$this->view()->render('login');
	}

	public function actionLogout()
	{
		if (($_POST['action'] ?? '') == 'logout')
		{
			if (($this->loggedUser->id ?? 0) <= 0)
			{
				redirect(baseUrl());
				exit;
			}

			\Holiday::$app->session->set('loggedUser',0, TRUE);
		
			$this->_messageStatus->addFlashMessage(\messageStatus::MESSAGE_SUCCESS, "logout", lang('Admin','logout_success'));
		}
		redirect(baseUrl());
	}

	public function actionProfile()
	{
		if (($this->loggedUser->id ?? 0) <= 0)
		{
			$this->_messageStatus->addFlashMessage(\messageStatus::MESSAGE_ERROR, "login", lang('Admin','must_login_first'));
			redirect(baseUrl().'administrator/user/login');
		}

		$editForm = new \app\models\administrator\ProfileForm;

		if (($_POST['action'] ?? '') == 'edit')
		{
			$data = [];
			
			$user = $this->userModel->findUnique([['where','id','=',($this->loggedUser->id ?? 0)]]);

			$fields = ['fullname','nickname','date_birth','gender'];

			foreach ($fields as $key)
			{
				$user->$key = $_POST[$key] ?? NULL;
				$data[$key] = $_POST[$key] ?? '';
			}
			
			$editForm->fillData($data);
			
			if ($editForm->runValidation())
			{
				if ($user->update('id',$fields))
				{
					$this->_messageStatus->addMessage(\messageStatus::MESSAGE_SUCCESS, "edit_profile", lang('Admin','edit_profile_success'));
					$this->loggedUser = $user;
				}else
					$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "edit_profile", lang('Admin','edit_profile_error'));
			} else
				$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "edit_profile", lang('Admin','edit_profil_data_invalid'));
		}

		$this->view()->render('profile', ['user'=>$this->loggedUser, 'editForm'=>&$editForm]);
	}


}
