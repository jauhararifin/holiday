<?php

namespace app\controllers;

class ArticleController extends \app\core\Controller {

	private $commentForm = NULL;
	private $article = NULL;

	public function __construct()
	{
		parent::__construct();
		$this->commentForm = new \app\models\CommentForm;
		\Holiday::$app->language->loadLanguage('Content');
		\helpers\Loader::load('ajax');
	}

	public function actionIndex()
	{
		$this->actionRead('');
	}

	public function actionRead(string $name)
	{
		$this->article = \app\models\ArticleModel::findUnique([
			['where','name','=',$name],
			['where','status','=',\app\models\ArticleModel::ARTICLE_PUBLIC],
		]);

		if ($this->article == NULL)
		{
			\Holiday::showError(lang('Content','article_not_found'), 404, 'error', lang('Content','article_error'));
		} else
		{
			if (isset($_POST['action']) && $_POST['action'] == 'comment')
				$this->sendComment();

			$comment_per_page = intval(configItem('comment.comments_per_page'));
			$number_of_comment = intval($this->article->count('comments'));
			$pageNow = $_GET['page'] ?? 1;
			$totalPage = $comment_per_page > 0 ? ceil($number_of_comment / $comment_per_page) : 0;

			if ($pageNow > $totalPage)
				$page = $totalPage;

			$commentOption = [
				'limit' => $comment_per_page,
				'offset' => ($pageNow-1)*$comment_per_page,
				'orderBy' => [
					'field'	=> 'time_created',
					'order'	=> 'ASC',
				],
				'criteria' => [
					['where','status','=',\app\models\CommentModel::COMMENT_PUBBLISH],
				],
			];
			$pagination = [
				'pageNow'	=> $pageNow,
				'totalPage'	=> $totalPage
			];

			if (isAjaxRequest())
			{
				$this->view()->print(json_encode([
					'article'	=> $this->article->toArray(),
				]));
			} else
			{
				$this->view()->render('article',[
					'article'		=>$this->article,
					'commentForm'	=>$this->commentForm,
					'comments'		=>$this->article->comments($commentOption),
					'pagination'	=>$pagination,
				]);
			}
		}
	}

	private function sendComment()
	{
		$fullname	= $_POST['fullname'] ?? '';
		$email		= $_POST['email'] ?? '';
		$content	= $_POST['content'] ?? '';

		$this->commentForm->fillData(['fullname' => $fullname, 'content' => $content, 'email' => $email]);

		if ($this->commentForm->runValidation())
		{
			$model = new \app\models\CommentModel;

			$fullname = $this->commentForm->getSanitizedData('fullname');
			$email = $this->commentForm->getSanitizedData('email');
			$content = $this->commentForm->getSanitizedData('content');

			if ($model->newComment($this->article->id(), $fullname, $email, $content))
			{
				$this->_messageStatus->addMessage(\messageStatus::MESSAGE_SUCCESS, "comment_validation", lang('Content','comment_sent'));
				$this->commentForm->resetData();
			}else
				$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "comment_validation", lang('Content','sending_comment_error'));
		}else
			$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "comment_validation", lang('Content','comment_invalid'));
	}

}
