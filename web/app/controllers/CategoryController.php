<?php

namespace app\controllers;

class CategoryController extends \app\core\Controller {

	private $category 		= NULL;

	public function __construct()
	{
		parent::__construct();
		\Holiday::$app->language->loadLanguage('Content');
		\helpers\Loader::load('ajax');
	}

	private function show()
	{
		if ($this->category === NULL)
		{
			\Holiday::showError(lang('Content','category_not_found'), 404);
			return;
		}

		$articles_per_page = intval(configItem('article.articles_per_page'));
		$number_of_articles = intval($this->category->count('articles',['criteria'=>[['where','status','=',\app\models\ArticleModel::ARTICLE_PUBLIC]]]));
		$pageNow = isset($_GET['page']) ? $_GET['page'] : 1;
		$totalPage = max(1, $articles_per_page > 0 ? ceil($number_of_articles / $articles_per_page) : 0);

		if ($pageNow > $totalPage)
		{
			\Holiday::showError(lang('Content','page_not_found'));
			return;
		}

		$articleOption = [
			'limit' => $articles_per_page,
			'offset' => ($pageNow-1)*$articles_per_page,
			'orderBy' => [
				'field'	=> 'time_updated',
				'order'	=> 'DESC',
			],
			'criteria' => [
				['where','status','=',\app\models\ArticleModel::ARTICLE_PUBLIC],
			],
		];

		$pagination = [
			'pageNow'	=> $pageNow,
			'totalPage'	=> $totalPage
		];

		if (isAjaxRequest())
		{
			$this->view()->print(json_encode([
				'category'=>$this->category->toArray(),
			]));
		} else
		{
			$this->view()->render('category',[
				'category'=>$this->category,
				'articles'=>$this->category->articles($articleOption),
				'pagination'=>$pagination,
			]);
		}
	}

	public function actionIndex()
	{
		$this->category = \app\models\CategoryModel::findUnique([],['id']);
		$this->show();
	}

	public function actionShow(string $name='')
	{
		if ((string) $name === '')
			$this->actionIndex();

		$this->category = \app\models\CategoryModel::findUnique([
			['where', 'name','=',(string) $name],
			['where', 'status','=', \app\models\CategoryModel::CATEGORY_PUBLIC]
		]);
		$this->show();
	}

}
