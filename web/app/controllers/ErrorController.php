<?php

namespace app\controllers;

class ErrorController extends \core\controller\ErrorController {

	public function __construct(string $message, int $statusCode, string $template, string $title, bool $return)
	{
		parent::__construct($message, $statusCode, $template, $title, $return);
	}

	public function actionShow()
	{
		$param = array(
			'message'	=> $this->message,
			'statusCode'=> $this->statusCode,
			'template' 	=> $this->template,
			'title' 	=> $this->title,
			'return' 	=> $this->return,
		);

		$this->view()->render('errors/'.$this->template, $param);
	}

}
