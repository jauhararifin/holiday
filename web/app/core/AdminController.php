<?php

namespace app\core;

class AdminController extends \app\core\Controller {

	protected $loggedUser = NULL;
	protected $userModel = NULL;

	public function __construct()
	{
		parent::__construct();
		
		$this->view()->setConfig('theme', 'admin');

		\Holiday::$app->language->loadLanguage('Admin');

		$this->userModel = new \app\models\UserModel;

		$userid = intval(\Holiday::$app->session->get('loggedUser'));
		$this->loggedUser = $this->userModel->findUnique([['where','id','=',($userid ?? 0)]]);

		if (($this->loggedUser->id ?? 0) <= 0)
		{
			$this->_messageStatus->addFlashMessage(\messageStatus::MESSAGE_ERROR, "auth_login", lang('Admin','must_login_first'));
			redirect(configItem('baseUrl').'administrator/user/login');
		}
	}

}
