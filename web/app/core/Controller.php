<?php

namespace app\core;

class Controller extends \core\Controller {

	protected $_messageStatus;

	public function __construct()
	{
		$this->_messageStatus = new \MessageStatus;
		$this->view()->addVars(array('messageStatus'=>$this->_messageStatus));
	}

}