<?php

return [

	'id'			 => "ID",
	'username'		 => "Username",
	'email'			 => "Email",
	'password'		 => "Password",
	'time_created'	 => "Created time",
	'time_updated'	 => "Updated time",
	'fullname'		 => "Full name",
	'nickname'		 => "Nick name",
	'date_birth'	 => "Birth date",
	'gender'		 => "Gender",
	'data'			 => "Data",
	'time_lastvisit' => "Last visit",

	'name'				=> "Name",
	'title'				=> "Title",

	'profile'			=> "Profile",
	'edit_profile'		=> "Edit profile",
	'category'			=> "Category",
	'create_category'	=> "Create category",
	'categories'		=> "Categories",
	'article'			=> "Article",
	'create_article'	=> "Create article",
	'articles'			=> "Articles",
	'login'				=> "Login",
	'logout'			=> "Logout",
	'author'			=> "Author",
	'updated'			=> "Updated", 
	'created'			=> "Created",
	'action'			=> "Action",
	'page'				=> "Page",
	'status'			=> "Status",
	'content'			=> "Content",
	'public'			=> "Public",
	'private'			=> "Private",
	'no_article'		=> "No article",
	'articles_in_category'	=> "Articles in category",

	'male'				=> "Male",
	'female'			=> "Female",

	'no_category'		=> "No category",

	'remember_me'		=> "Remember me",

	'categories_per_page'	=> "Categories per page",
	'articles_per_page'		=> "Articles per page",

	'login_success'				=> "Login success",
	'wrong_username_password'	=> "Wrong username or password",
	'login_data_invalid'		=> "Login data invalid",

	'logout_success'	=> "Logout success",
	'must_login_first'	=> "You have to login first",

	'edit_profile_success'		=> "Edit profile success",
	'edit_profile_error'		=> "Edit profile error",
	'edit_profil_data_invalid'	=> "Edit profile data invalid",

	'category_delete_success'	=> "Category deleted",
	'category_delete_error'		=> "Error when deleting category",

	'category_create_success'		=> "Category created", 
	'category_create_error'			=> "Error when creating category",
	'category_name_already_exists'	=> "Category name already exists",
	'category_create_data_invalid'	=> "Category data invalid",

	'article_delete_success'		=> "Article deleted",
	'article_delete_error'			=> "Error when deleting article",

	'article_create_success'		=> "Article created", 
	'article_create_error'			=> "Error when creating article",
	'article_name_already_exists'	=> "article name already exists",
	'article_create_data_invalid'	=> "article data invalid",

];