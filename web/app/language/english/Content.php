<?php

return array(

	'article_error'			=> "Article error.",
	'article_not_found'		=> "Article not found.",
	'comment_sent'			=> "Comment is sent.",
	'sending_comment_error'	=> "Error in sending comment.",
	'comment_invalid'		=> "Comment data is not valid.",

	'page_not_found'		=> "Page not found.",
	'category_not_found'	=> "Category not found.",

	'id'					=> "ID",
	'name'					=> "Name",
	'title'					=> "Title",
	'author_id'				=> "Author ID",
	'time_created'			=> "Created",
	'time_updated'			=> "Updated",
	'time_published'		=> "Published",
	'content'				=> "Content",
	'status'				=> "Status",

	'article_id'			=> "Article ID",
	'fullname'				=> "Full name",
	'email'					=> "Email",

	'username'				=> "Username",
	'password'				=> "Password",
	'salt'					=> "Salt",
	'nickname'				=> "Nickname",
	'date_birth'			=> "Birth date",
	'gender'				=> "Gender",
	'data'					=> "Data",
	'time_lastvisit'		=> "Last visit",

);