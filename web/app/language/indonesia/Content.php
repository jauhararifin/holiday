<?php

return array(

	'article_error'			=> "Artikel error.",
	'article_not_found'		=> "Artikel tidak ditemukan.",
	'comment_sent'			=> "Komentar terkirim.",
	'sending_comment_error'	=> "Gagal mengirim komentar.",
	'comment_invalid'		=> "Data komentar tidak valid.",

	'page_not_found'		=> "Halaman tidak ditemukan.",
	'category_not_found'	=> "Kategori tidak ditemukan.",

	'id'					=> "ID",
	'name'					=> "Nama",
	'title'					=> "Judul",
	'author_id'				=> "ID Author",
	'time_created'			=> "Tanggal dibuat",
	'time_updated'			=> "Tanggal diupdate",
	'time_published'		=> "Tanggal dipublish",
	'content'				=> "Isi",
	'status'				=> "Status",

	'article_id'			=> "ID Artikel",
	'fullname'				=> "Nama lengkap",
	'email'					=> "Email",

	'username'				=> "Username",
	'password'				=> "Kata sandi",
	'salt'					=> "Salt",
	'nickname'				=> "Nama panggilan",
	'date_birth'			=> "Tanggal lahir",
	'gender'				=> "Jenis kelamin",
	'data'					=> "Data",
	'time_lastvisit'		=> "Terakhir berkunjung",

);