<?php

namespace app\models;

class ArticleModel extends \core\model\ActiveRecord {

	const ARTICLE_PUBLIC	= 1;
	const ARTICLE_DRAFT		= 0;

	public function __construct()
	{
		parent::__construct();
		\Holiday::$app->language->loadLanguage('Content');
		foreach (self::$columns as $key => $value)
			self::$columns[$key]['label'] = lang('Content', $key);
	}

	public static $className = __CLASS__;

	public static $tableName = 'articles';

	public static $relations = array(
		'author' => [
			'type'=>\core\model\ActiveRecord::HAS_ONE,
			'domesticKey' => 'author_id',
			'foreignClass' => '\\app\\models\\UserModel',
			'foreignKey' => 'id'
		],
		'categories' => [
			'type'=>\core\model\ActiveRecord::MANY_MANY,
			'domesticKey' => array('id', 'article_id'),
			'connectionTable' => 'category_article',
			'foreignKey' => array('category_id', 'id'),
			'foreignClass' => '\\app\\models\\CategoryModel',
		],
		'comments' => [
			'type'=>\core\model\ActiveRecord::HAS_MANY,
			'domesticKey' => 'id',
			'foreignClass' => '\\app\\models\\CommentModel',
			'foreignKey' => 'article_id',
		],
	);

	public static $columns = array(
		'id' => array(
			'label' => "ID",
		),
		'name' => array(
			'label' => "Name",
		),
		'title' => array(
			'label' => "Title",
		),
		'author_id' => array(
			'label' => "Authod ID",
		),
		'time_created' => array(
			'label' => "Created",
		),
		'time_updated' => array(
			'label' => "Updated",
		),
		'time_published' => array(
			'label' => "Published",
		),
		'content' => array(
			'label' => "Content",
		),
		'status' => array(
			'label' => "Status",
		),
	);

	public function time_created($format=NULL)
	{
		if ((string) $format === '')
			$format = 'd F Y';
		$date = new \DateTime(parent::time_created());
		return $date->format((string) $format);
	}

	public function time_updated($format=NULL)
	{
		if ((string) $format === '')
			$format = 'd F Y';
		$date = new \DateTime(parent::time_updated());
		return $date->format((string) $format);
	}

	public function time_published($format=NULL)
	{
		if ((string) $format === '')
			$format = 'd F Y';
		$date = new \DateTime(parent::time_published());
		return $date->format((string) $format);
	}

	public function url()
	{
		return baseUrl().'article/'.$this->name();
	}

	public function commentCount($public=TRUE)
	{
		if ($public === TRUE)
			return $this->count('comments', ['criteria'=> [['where','status','=',\app\models\CommentModel::COMMENT_PUBBLISH]]]);
		else
			return $this->count('comments');
	}

}
