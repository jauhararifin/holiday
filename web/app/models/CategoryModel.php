<?php

namespace app\models;

class CategoryModel extends \core\model\ActiveRecord {

	const CATEGORY_PUBLIC	= 1;
	const CATEGORY_PRIVATE	= 0;

	public function __construct()
	{
		parent::__construct();
		\Holiday::$app->language->loadLanguage('Content');
		foreach (self::$columns as $key => $value)
			self::$columns[$key]['label'] = lang('Content', $key);
	}

	public static $className = __CLASS__;

	public static $tableName = 'categories';

	public static $relations = array(
		'author' => array(
			'type'=>\core\model\ActiveRecord::HAS_ONE,
			'domesticKey' => 'author_id',
			'foreignClass' => '\\app\\models\\UserModel',
			'foreignKey' => 'id'
		),
		'articles' => array(
			'type'=>\core\model\ActiveRecord::MANY_MANY,
			'domesticKey' => array('id', 'category_id'),
			'connectionTable' => 'category_article',
			'foreignKey' => array('article_id', 'id'),
			'foreignClass' => '\\app\\models\\ArticleModel',
		),
	);

	public static $columns = array (
		'id' => array(
			'label' => "ID",
		),
		'name' => array(
			'label' => "Name",
		),
		'title' => array(
			'label' => "Title",
		),
		'author_id' => array(
			'label' => "Author ID",
		),
		'time_created' => array(
			'label' => "Created",
		),
		'time_updated' => array(
			'label' => "Updated",
		),
		'status' => array(
			'label' => "Status",
		),
	);

	public function url()
	{
		return baseUrl().'category/'.$this->name();
	}

	public function addArticle(int $id)
	{
		$article = \app\models\ArticleModel::findUnique([['where','id','=',$id]]);
		if ($article != NULL && ($article->id ?? 0) == $id)
		{
			$tableName = self::$relations['articles']['connectionTable'];
			$categoryKey = self::$relations['articles']['domesticKey'][1];
			$articleKey = self::$relations['articles']['foreignKey'][0];
			if ($this->db($tableName)->where($categoryKey,'=',$this->id)->where($articleKey,'=',$article->id)->count() <= 0)
			{
				return $this->db($tableName)->insert([
					$categoryKey => $this->id,
					$articleKey => $id,
				]);
			}
			return FALSE;
		}
		return FALSE;
	}

	public function removeArticle(int $id)
	{
		$tableName = self::$relations['articles']['connectionTable'];
		$categoryKey = self::$relations['articles']['domesticKey'][1];
		$articleKey = self::$relations['articles']['foreignKey'][0];
		if ($this->db($tableName)->where($categoryKey,'=',$this->id)->where($articleKey,'=',$id)->count() > 0)
		{
			return $this->db($tableName)->where($categoryKey,'=',$this->id)->where($articleKey,'=',$id)->delete();
		}
		return FALSE;
	}

}
