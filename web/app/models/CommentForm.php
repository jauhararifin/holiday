<?php

namespace app\models;

class CommentForm extends \core\model\FormModel {

	public function __construct()
	{
		parent::__construct();
    	\Holiday::$app->language->loadLanguage('Content');
    	foreach ($this->attributes as $key => $value)
			$this->attributes[$key]['label'] = lang('Content', $key);
	}

	public $className = __CLASS__;

	public $attributes = [
		'fullname' => [
			'label' => "Fullname",
	    	'validations' => [
	        	['Required'],
	        	['MaxLen',100],
	        	['ValidName'],
	    	],
			'sanitations' => [
				'Htmlencode',
			],
		],
    	'email' => [
    		'label' => "Email",
    		'validations' => [
				['Required'],
				['ValidEmail'],
			],
			'sanitations' => [
				'Htmlencode',
			],
	  	],
	    'content' => [
			'label' => "Content",
      		'validations' => [
    			['Required'],
      		],
      		'sanitations' => [
        		'Htmlencode',
      		],
		],
	];

}
