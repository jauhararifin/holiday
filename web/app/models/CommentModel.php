<?php

namespace app\models;

class CommentModel extends \core\model\ActiveRecord {

	const COMMENT_PUBBLISH	= 2;
	const COMMENT_HIDDEN	= 1;

	public function __construct()
	{
		parent::__construct();
		\Holiday::$app->language->loadLanguage('Content');
		foreach (self::$columns as $key => $value)
			self::$columns[$key]['label'] = lang('Content', $key);
	}

	public static $className = __CLASS__;

	public static $tableName = 'comments';

	public static $relations = array(
			'article' => array(
				'type'=>\core\model\ActiveRecord::HAS_ONE,
				'domesticKey' => 'article_id',
				'foreignClass' => '\\app\\models\\ArticleModel',
				'foreignKey' => 'id'
			),
		);

	public static $columns = array(
		'id' => array(
			'label' => "ID", //lang('Content','fullname'),
		),
		'article_id' => array(
			'label' => "Article ID", //lang('Content','article_id'),  
		),
		'fullname' => array(
			'label' => "Full name", //lang('Content','fullname'),  
		),
		'email'	=> array(
			'label' => "Email", //lang('Content','email'), 
		),
		'time_created' => array(
			'label' => "Created", //lang('Content','time_created'),  
		),
		'content' => array(
			'label' => "Content", //lang('Content','content'), 
		),
		'status' => array(
			'label' => "Label", //lang('Content','status'), 
		),
	);

	public function newComment($article_id, $fullname, $email, $content)
	{
		if (!is_numeric($article_id) || (string) $fullname == '' || (string) $email == '' || (string) $content == '')
			return FALSE;
		$this->article_id = $article_id;
		$this->fullname = (string) $fullname;
		$this->email = (string) $email;
		$this->content = (string) $content;
		$this->time_created = date('Y-m-d H:i:s');
		$this->status = intval(configItem('comment.default_status'));
		return intval($this->insert()) > 0;
	}

}
