<?php

namespace app\models;

class LoginForm extends \core\model\FormModel {

	public function __construct()
	{
		parent::__construct();
    \Holiday::$app->language->loadLanguage('Admin');
    foreach ($this->attributes as $key => $value)
			$this->attributes[$key]['label'] = lang('Admin', $key);
	}

	public $className = __CLASS__;

	public $attributes = [
		'username' => [
			'label' => "Username",
	    	'validations' => [
	        	['Required'],
	        	['MaxLen',32],
	    	],
			'sanitations' => [
				'Htmlencode',
			],
		],
		'password' => [
	  		'label' => "Password",
	    	'validations' => [
				['Required'],
				['MaxLen', 32],
			],
			'sanitations' => [
				'Htmlencode',
			],
		],
	];

}
