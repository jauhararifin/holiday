<?php

namespace app\models;

class UserModel extends \core\model\ActiveRecord {

	public function __construct()
	{
		parent::__construct();
		\Holiday::$app->language->loadLanguage('Content');
		foreach (self::$columns as $key => $value)
			self::$columns[$key]['label'] = lang('Content', $key);
	}

	public static $className = __CLASS__;

	public static $tableName = 'users';

	public static $relations = [
		'article' => [
			'type'=>\core\model\ActiveRecord::HAS_MANY,
			'domesticKey' => 'id',
			'foreignClass' => '\\app\\models\\ArticleModel',
			'foreignKey' => 'author_id',
		],
		'category' => [
			'type'=>\core\model\ActiveRecord::HAS_MANY,
			'domesticKey' => 'id',
			'foreignClass' => '\\app\\models\\CategoryModel',
			'foreignKey' => 'author_id',
		],
	];

	public static $primaryKey = 'id';

	public static $columns = [
		'id' => [
			'label' => "ID",
		],
		'username' => [
			'label' => "Username",
		],
		'email' => [
			'label' => "Email",
		],
		'password' => [
			'label' => "Password",
		],
		'time_created' => [
			'label' => "Created",
		],
		'time_updated' => [
			'label' => "Updated",
		],
		'fullname' => [
			'label' => "Full name",
		],
		'nickname' => [
			'label' => "Nickname",
		],
		'date_birth' => [
			'label' => "Birth date",
		],
		'gender' => [
			'label' => "Gender",
		],
		'data' => [
			'label' => "Data",
		],
		'time_lastvisit' => [
			'label' => "Last visit",
		],
	];

}
