<?php

namespace app\models\administrator;

class ArticleForm extends \core\model\FormModel {

	public function __construct()
	{
		parent::__construct();
    	\Holiday::$app->language->loadLanguage('Admin');
    	foreach ($this->attributes as $key => $value)
			$this->attributes[$key]['label'] = lang('Admin', $key);
	}

	public $className = __CLASS__;

	public $attributes = [
		'name' => [
			'label' => "Name",
	    	'validations' => [
	        	['Required'],
	        	['MaxLen',32],
	        	['AlphanumericDash'],
	    	],
			'sanitations' => [],
		],

	  	'title' => [
	  		'label' => "Title",
	    	'validations' => [
				['Required'],
				['MaxLen', 100],
			],
			'sanitations' => [],
	  	],

	  	'content' => [
	  		'label' => "Content",
	  		'validations' => [],
	  		'sanitations' => [
	  			'xssClean'
	  		],
	  	],

	  	'status' => [
	  		'label' => "Status",
	  		'validations' => [
	  			['Required'],
	  			['GreaterThanEqualTo',0],
	  			['LessThanEqualTo',1],
	  		],
	  		'sanitations' => [],
	  	]

	];

}
