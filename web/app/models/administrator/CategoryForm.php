<?php

namespace app\models\administrator;

class CategoryForm extends \core\model\FormModel {

	public function __construct()
	{
		parent::__construct();
    	\Holiday::$app->language->loadLanguage('Admin');
    	foreach ($this->attributes as $key => $value)
			$this->attributes[$key]['label'] = lang('Admin', $key);
	}

	public $className = __CLASS__;

	public $attributes = [
		'name' => [
			'label' => "Name",
	    	'validations' => [
	        	['Required'],
	        	['MaxLen',32],
	    	],
			'sanitations' => [
				'Htmlencode',
			],
		],
	  	'title' => [
	  		'label' => "Title",
	    	'validations' => [
				['Required'],
				['MaxLen', 100],
			],
			'sanitations' => [
				'Htmlencode',
			],
	  	],
	  	'status' => [
	  		'label' => "Status",
	  		'validations' => [
	  			['Required'],
	  			['GreaterThanEqualTo',0],
	  			['LessThanEqualTo',1],
	  		],
	  		'sanitations' => [],
	  	]
	];

}
