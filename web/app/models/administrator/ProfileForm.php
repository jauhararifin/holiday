<?php

namespace app\models\administrator;

class ProfileForm extends \core\model\FormModel {

	public function __construct()
	{
		parent::__construct();
    	\Holiday::$app->language->loadLanguage('Admin');
    	foreach ($this->attributes as $key => $value)
			$this->attributes[$key]['label'] = lang('Admin', $key);
	}

	public $className = __CLASS__;

	public $attributes = [
		'fullname' => [
			'label' => "Full name",
	    	'validations' => [
	        	['Required'],
	        	['MaxLen',100],
	        	['ValidName'],
	    	],
			'sanitations' => [
				'Htmlencode',
			],
		],
		'nickname' => [
			'label' => "Nick name",
	    	'validations' => [
	        	['Required'],
	        	['MaxLen',100],
	        	['ValidName'],
	    	],
			'sanitations' => [
				'Htmlencode',
			],
		],
		'date_birth' => [
			'label' => "Birth date",
			'validations' => [
				['Required'],
				['RegexMatch', '/[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}/'],
			],
		],
		'gender' => [
			'label' => 'Gender',
			'validations' => [
				['InList',[0,1]],
			],
		],
	];

}
