<?php $this->render('header', array('title'=>configItem('siteName').' | Articles')); ?>

	<div class="container">
		<div class="col-md-12">
			
			<form id="form-pagination" class="form-inline" method="get" action="<?php echo baseUrl().'administrator/article' ?>">

				<div class="form-group">
					<label><?php htmlLang('Admin','articles_per_page'); ?></label>
					<input name="articlesPerPage" type="text" class="form-control" style="width:60px;" value="<?php if (isset($articlesPerPage)) echo $articlesPerPage; ?>">
				</div>

				<div class="form-group pull-right">
					<label><?php htmlLang('Admin','page'); ?></label>
					<input name="currentPage" type="text" class="form-control" style="width:60px;" value="<?php if (isset($currentPage)) echo $currentPage; ?>">
					<button style="display:none;"></button>
					<div class="input-group btn-group">
						<button id="prev-page" class="input-group-addon" <?php if($currentPage <= 1) echo 'disabled'; ?>><span class="glyphicon glyphicon-chevron-left"></span></button>
						<button id="next-page" class="input-group-addon" <?php if($currentPage >= $totalPage) echo 'disabled'; ?>><span class="glyphicon glyphicon-chevron-right"></span></button>
					</div>
				</div>

				<script>
					$(document).ready(function(){
						$('input[name=articlesPerPage], input[name=currentPage]').keydown(function(event) {
					    	if (event.keyCode == 13)
					    		this.form.submit();
						});
						$("#next-page").click(function(){
							$("input[name=currentPage]").val((parseInt($("input[name=currentPage]").val()) + 1));
							$("#form-pagination").submit();
						});
						$("#prev-page").click(function(){
							$("input[name=currentPage]").val((parseInt($("input[name=currentPage]").val()) - 1));
							$("#form-pagination").submit();
						});
					});
				</script>
			
			</form>

			<hr>

			<div class="panel panel-default">
				<div class="panel-heading">
					<?php htmlLang('Admin','articles'); ?>
				</div>

				<table class="table">
					<tr>
						<th><?php htmlLang('Admin','id'); ?></th>
						<th><?php htmlLang('Admin','title'); ?></th>
						<th><?php htmlLang('Admin','author'); ?></th>
						<th><?php htmlLang('Admin','updated'); ?></th>
						<th><?php htmlLang('Admin','action'); ?></th>
					</tr>
					<?php if (count($articles) > 0) : ?>
					<?php foreach ($articles as $article): ?>
					<tr data-article-id="<?php echo $article->id; ?>">
						<td><?php html($article->id); ?></td>
						<td><?php html($article->title); ?></td>
						<td><?php html($article->author->fullname); ?></td>
						<td><?php html($article->time_updated); ?></td>
						<td>
							<a style="color:white" href="<?php echo baseUrl().'administrator/article/edit/'.$article->id; ?>"><button class="btn btn-sm btn-success"><span class="glyphicon glyphicon-pencil"></span></button></a>
							<button class="btn btn-sm btn-danger delete-article"><span class="glyphicon glyphicon-remove"></span></button>
						</td>
					</tr>
					<?php endforeach; ?>
					<?php else: ?>
						<tr>
							<td colspan="5"><?php htmlLang('Admin','no_article'); ?></td>
						</tr>
					<?php endif; ?>
				</table>

				<form id="form-delete" method="post" action="<?php echo baseUrl().'administrator/article/delete'; ?>">
					<input type="hidden" name="action" value="delete">
					<input type="hidden" name="<?php echo csrfTest(); ?>" value="<?php echo csrfToken(); ?>">
					<input type="hidden" name="id">
				</form>
				<script>
					$(document).ready(function(){
						$(".delete-article").click(function() {
							$("#form-delete input[name=id]").val((parseInt($(this).parent().parent().attr('data-article-id'))));
							$("#form-delete").submit();
						});
					});
				</script>
			</div>

		</div>
	</div>

<?php $this->render('footer'); ?>