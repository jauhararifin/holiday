<?php $this->render('header', array('title'=>configItem('siteName').' | Categories')); ?>

	<div class="container">
		<div class="col-md-12">

			<form id="form-pagination" class="form-inline" method="get" action="<?php echo baseUrl().'administrator/category' ?>">

				<div class="form-group">
					<label><?php htmlLang('Admin','categories_per_page'); ?></label>
					<input name="categoriesPerPage" type="text" class="form-control" style="width:60px;" value="<?php if (isset($categoriesPerPage)) echo $categoriesPerPage; ?>">
				</div>

				<div class="form-group pull-right">
					<label><?php htmlLang('Admin','page'); ?></label>
					<input name="currentPage" type="text" class="form-control" style="width:60px;" value="<?php if (isset($currentPage)) echo $currentPage; ?>">
					<button style="display:none;"></button>
					<div class="input-group btn-group">
						<button id="prev-page" class="input-group-addon" <?php if($currentPage <= 1) echo 'disabled'; ?>><span class="glyphicon glyphicon-chevron-left"></span></button>
						<button id="next-page" class="input-group-addon" <?php if($currentPage >= $totalPage) echo 'disabled'; ?>><span class="glyphicon glyphicon-chevron-right"></span></button>
					</div>
				</div>

				<script>
					$(document).ready(function(){
						$('input[name=categoriesPerPage], input[name=currentPage]').keydown(function(event) {
					    	if (event.keyCode == 13)
					    		this.form.submit();
						});
						$("#next-page").click(function(){
							$("input[name=currentPage]").val((parseInt($("input[name=currentPage]").val()) + 1));
							$("#form-pagination").submit();
						});
						$("#prev-page").click(function(){
							$("input[name=currentPage]").val((parseInt($("input[name=currentPage]").val()) - 1));
							$("#form-pagination").submit();
						});
					});
				</script>
			
			</form>

			<hr>

			<div class="panel panel-default">
				<div class="panel-heading">
					<?php htmlLang('Admin','categories'); ?>
				</div>

				<table class="table">
					<tr>
						<th><?php htmlLang('Admin','id'); ?></th>
						<th><?php htmlLang('Admin','title'); ?></th>
						<th><?php htmlLang('Admin','author'); ?></th>
						<th><?php htmlLang('Admin','updated'); ?></th>
						<th><?php htmlLang('Admin','action'); ?></th>
					</tr>
					<?php if (count($categories) > 0) : ?>
					<?php foreach ($categories as $category): ?>
					<tr data-category-id="<?php echo $category->id; ?>">
						<td><?php html($category->id); ?></td>
						<td><?php html($category->title); ?></td>
						<td><?php html($category->author->fullname); ?></td>
						<td><?php html($category->time_updated); ?></td>
						<td>
							<a style="color:white" href="<?php echo baseUrl().'administrator/category/edit/'.$category->id; ?>"><button class="btn btn-sm btn-success"><span class="glyphicon glyphicon-pencil"></span></button></a>
							<button class="btn btn-sm btn-danger delete-category"><span class="glyphicon glyphicon-remove"></span></button>
						</td>
					</tr>
					<?php endforeach; ?>
					<?php else: ?>
						<tr>
							<td colspan="5"><?php htmlLang('Admin','no_category'); ?></td>
						</tr>
					<?php endif; ?>
				</table>

				<form id="form-delete" method="post" action="<?php echo baseUrl().'administrator/category/delete'; ?>">
					<input type="hidden" name="action" value="delete">
					<input type="hidden" name="<?php echo csrfTest(); ?>" value="<?php echo csrfToken(); ?>">
					<input type="hidden" name="id">
				</form>
				<script>
					$(document).ready(function(){
						$(".delete-category").click(function() {
							$("#form-delete input[name=id]").val((parseInt($(this).parent().parent().attr('data-category-id'))));
							$("#form-delete").submit();
						});
					});
				</script>
			</div>

		</div>
	</div>

<?php $this->render('footer'); ?>