<?php $this->render('header', array('title'=>configItem('siteName').' | Create Category')); ?>

	<div class="container">
		<div class="col-md-12">

			<form class="form-horizontal" method="post" action="<?php echo baseUrl().'administrator/category/create'; ?>">

				<input type="hidden" name="action" value="create">
				<input type="hidden" name="<?php echo csrfTest(); ?>" value="<?php echo csrfToken(); ?>">

				<?php
	                $nameErr = $categoryForm->validationError('name');
	                $nameStat = $categoryForm->attributeStatus('name');
	            ?>
				<div class="form-group <?php if ($nameStat == \InputValidation::SUCCESS) echo 'has-success'; else if ($nameStat == \InputValidation::VALIDATION_ERROR) echo 'has-error'; ?>">
					<label class="col-sm-2 control-label"><?php htmlLang('Admin','name'); ?></label>
					<div class="col-sm-10">
						<input name="name" type="text" class="form-control" value="<?php html($categoryForm->getData('name')); ?>" placeholder="<?php htmlLang('Admin','name'); ?>">
						<?php if ($nameStat == \InputValidation::VALIDATION_ERROR): ?>
			            <span class="help-block"><?php html($nameErr[0]); ?></span>
			            <?php endif; ?>
					</div>
				</div>

				<?php
	                $titleErr = $categoryForm->validationError('title');
	                $titleStat = $categoryForm->attributeStatus('title');
	            ?>
				<div class="form-group <?php if ($titleStat == \InputValidation::SUCCESS) echo 'has-success'; else if ($titleStat == \InputValidation::VALIDATION_ERROR) echo 'has-error'; ?>">
					<label class="col-sm-2 control-label"><?php htmlLang('Admin','title'); ?></label>
					<div class="col-sm-10">
						<input name="title" type="text" class="form-control" value="<?php html($categoryForm->getData('title')); ?>" placeholder="<?php htmlLang('Admin','title'); ?>">
						<?php if ($titleStat == \InputValidation::VALIDATION_ERROR): ?>
			            <span class="help-block"><?php html($titleErr[0]); ?></span>
			            <?php endif; ?>
					</div>
				</div>

				<?php
	                $statusErr = $categoryForm->validationError('status');
	                $statusStat = $categoryForm->attributeStatus('status');
	            ?>
				<div class="form-group  <?php if ($statusStat == \InputValidation::SUCCESS) echo 'has-success'; else if ($statusStat == \InputValidation::VALIDATION_ERROR) echo 'has-error'; ?>">
					<label class="col-sm-2 control-label"><?php htmlLang('Admin', 'status'); ?></label>
					<div class="col-sm-10">
						<select name="status" class="form-control">
							<option value="1" <?php if (intval($categoryForm->getData('status')) == 1) echo 'selected'; ?>><?php htmlLang('Admin','public'); ?></option>
							<option value="0" <?php if (intval($categoryForm->getData('status')) == 0) echo 'selected'; ?>><?php htmlLang('Admin','private'); ?></option>
						</select>
						<?php if ($statusStat == \InputValidation::VALIDATION_ERROR): ?>
			            <span class="help-block"><?php html($statusErr[0]); ?></span>
			            <?php endif; ?>
					</div>
				</div>

				<hr>

				<button type="submit" class="btn btn-primary pull-right"><?php htmlLang('Admin','create_category'); ?></button>

			</form>

		</div>
	</div>

<?php $this->render('footer'); ?>