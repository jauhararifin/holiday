<?php $this->render('header', array('title'=>configItem('siteName').' | Edit Category')); ?>

	<div class="container">
		<div class="col-md-12">

			<form class="form-horizontal" method="post" action="<?php echo baseUrl().'administrator/category/edit/'.$category->id; ?>">

				<input type="hidden" name="action" value="edit">
				<input type="hidden" name="<?php echo csrfTest(); ?>" value="<?php echo csrfToken(); ?>">

				<div class="form-group">
					<label class="col-sm-2 control-label"><?php htmlLang('Admin','name'); ?></label>
					<div class="col-sm-10">
						<input type="text" class="form-control" value="<?php html($category->name); ?>" placeholder="Identifier Name" disabled>
					</div>
				</div>

				<?php
	                $titleErr = $categoryForm->validationError('title');
	                $titleStat = $categoryForm->attributeStatus('title');
	            ?>
				<div class="form-group <?php if ($titleStat == \InputValidation::SUCCESS) echo 'has-success'; else if ($titleStat == \InputValidation::VALIDATION_ERROR) echo 'has-error'; ?>">
					<label class="col-sm-2 control-label"><?php htmlLang('Admin','title'); ?></label>
					<div class="col-sm-10">
						<input name="title" type="text" class="form-control" value="<?php html($categoryForm->getData('title') ?? $category->title); ?>" placeholder="Title">
						<?php if ($titleStat == \InputValidation::VALIDATION_ERROR): ?>
			            <span class="help-block"><?php html($titleErr[0]); ?></span>
			            <?php endif; ?>
					</div>
				</div>

				<?php
	                $statusErr = $categoryForm->validationError('status');
	                $statusStat = $categoryForm->attributeStatus('status');
	            ?>
				<div class="form-group  <?php if ($statusStat == \InputValidation::SUCCESS) echo 'has-success'; else if ($statusStat == \InputValidation::VALIDATION_ERROR) echo 'has-error'; ?>">
					<label class="col-sm-2 control-label"><?php htmlLang('Admin', 'status'); ?></label>
					<div class="col-sm-10">
						<select name="status" class="form-control">
							<option value="1" <?php if (intval($categoryForm->getData('status') ?? $category->status) == 1) echo 'selected'; ?>><?php htmlLang('Admin','public'); ?></option>
							<option value="0" <?php if (intval($categoryForm->getData('status') ?? $category->status) == 0) echo 'selected'; ?>><?php htmlLang('Admin','private'); ?></option>
						</select>
						<?php if ($statusStat == \InputValidation::VALIDATION_ERROR): ?>
			            <span class="help-block"><?php html($statusErr[0]); ?></span>
			            <?php endif; ?>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label">&nbsp;</label>
					<div class="col-sm-10">
						<button type="submit" class="btn btn-primary pull-right">Edit Category</button>
					</div>
				</div>

			</form>

			<div class="row">
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php htmlLang('Admin','articles'); ?> <span id="article-load-indicator" class="glyphicon glyphicon-hourglass"></span>
						</div>

						<table id="available-articles" class="table">
							<tr>
								<th><?php htmlLang('Admin','id'); ?></th>
								<th><?php htmlLang('Admin','title'); ?></th>
								<th>&nbsp;</th>
							</tr>
						</table>

						<nav>
							<ul class="pager pull-right">
								<button class="btn btn-default" id="prev-available-article"><span class="glyphicon glyphicon-chevron-left"></span></button>
								<button class="btn btn-default" id="next-available-article"><span class="glyphicon glyphicon-chevron-right"></span></button>
							</ul>
						</nav>
					</div>
				</div>

				<form id="add-article-form" method="post" action="<?php echo baseUrl().'administrator/category/edit/'.$category->id; ?>">
					<input type="hidden" name="<?php echo csrfTest(); ?>" value="<?php echo csrfToken(); ?>">
					<input type="hidden" name="action" value="addArticle">
					<input type="hidden" name="id" value="">
				</form>

				<script>
					var totalPage = 2;
					var currentPage = 1;

					function setAvailableArticle(page) {
						$("#article-load-indicator").show();
						if (typeof(page) == "undefined") page = 1;
						$.get("<?php echo baseUrl().'administrator/article'; ?>?page="+page.toString(), function (data) {
							var articles = data.articles;
							totalPage = data.totalPage;
							currentPage = data.currentPage;
							
							$(".available-article").remove();
							
							if (articles.length > 0) {
								for (var i = 0; i < articles.length; i++) {
									str = "<tr class=\"available-article\" data-article-id=\""+articles[i].id+"\">";
									str += "<td>"+articles[i].id+"</td>";
									str += "<td>"+articles[i].title+"</td>";
									str += "<td><button class=\"add-article-btn btn btn-primary\"><span class=\"glyphicon glyphicon-chevron-right\"></span></button></td>";
									str += "</tr>";
									$("#available-articles").append(str);
								}
							} else {
								str = "<tr>	<td colspan=\"3\"><?php htmlLang('Admin','no_article'); ?></td></tr>";
								$("#available-articles").append(str);
							}

							$(".add-article-btn").click(function(){
								var id = parseInt($(this).parent().parent().attr("data-article-id"));
								$("#add-article-form input[name=id]").val(id);
								$("#add-article-form").submit();
							});

							if (currentPage <= 1)
								$("#prev-available-article").attr("disabled",true);
							else
								$("#prev-available-article").removeAttr("disabled");
							
							if (currentPage >= totalPage)
								$("#next-available-article").attr("disabled",true);
							else
								$("#next-available-article").removeAttr("disabled");
						}, "json").always(function(){
							$("#article-load-indicator").hide();
						});
					}

					$("#next-available-article").click(function(){
						setAvailableArticle(currentPage+1);
					});

					$("#prev-available-article").click(function(){
						setAvailableArticle(currentPage-1);
					});

					setAvailableArticle();
				</script>

				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php htmlLang('Admin','articles_in_category'); ?>
						</div>

						<table class="table">
							<tr>
								<th><?php htmlLang('Admin','id'); ?></th>
								<th><?php htmlLang('Admin','title'); ?></th>
								<th>&nbsp;</th>
							</tr>
							<?php if (count($category->articles) > 0) : ?>
							<?php foreach ($category->articles as $article): ?>
							<tr data-article-id="<?php echo $article->id; ?>">
								<td><?php html($article->id); ?></td>
								<td><?php html($article->title); ?></td>
								<td><button class="remove-article-btn btn btn-danger"><span class="glyphicon glyphicon-remove"></span></button></td>
							</tr>
							<?php endforeach; ?>
							<?php else: ?>
								<tr>
									<td colspan="3"><?php htmlLang('Admin','no_article'); ?></td>
								</tr>
							<?php endif; ?>
						</table>
					</div>
				</div>

				<form id="remove-article-form" method="post" action="<?php echo baseUrl().'administrator/category/edit/'.$category->id; ?>">
					<input type="hidden" name="<?php echo csrfTest(); ?>" value="<?php echo csrfToken(); ?>">
					<input type="hidden" name="action" value="removeArticle">
					<input type="hidden" name="id" value="">
				</form>

				<script>
					$(".remove-article-btn").click(function(){
						var id = parseInt($(this).parent().parent().attr("data-article-id"));
						$("#remove-article-form input[name=id]").val(id);
						$("#remove-article-form").submit();
					});
				</script>

			</div>

		</div>
	</div>

<?php $this->render('footer'); ?>