    <script type="text/javascript" src="<?php echo $this->asset('js/noty/packaged/jquery.noty.packaged.min.js'); ?>"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        <?php foreach ($messageStatus->getMessages() as $type => $messages): ?>
        <?php foreach ($messages as $mess): ?>
        noty({
          text  :"<?php html($mess); ?>",
          type  :"<?php echo $type; ?>",
          dismissQueue: true,
          layout:'bottomRight',
        });
        <?php endforeach; ?>
        <?php endforeach; ?>

      });
    </script>

	</body>
</html>