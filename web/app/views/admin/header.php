<!DOCTYPE html>
<html>
	<head>
		<title><?php html($title) ?? ''; ?></title>

		<link rel="stylesheet" type="text/css" href="<?php echo $this->asset('css/bootstrap.min.css'); ?>">
		<script src="<?php echo $this->asset('js/jquery.min.js'); ?>"></script>
		<script src="<?php echo $this->asset('js/bootstrap.min.js'); ?>"></script>

		<style>
			.panel {
				border-radius: 0;
			}
			.panel .panel-heading {
				background-color: #337ab7;
				color: white;
			}
		</style>
	</head>
	<body>

	<form id="logout-frm" method="post" action="<?php echo baseUrl().'administrator/user/logout'; ?>">
		<input type="hidden" name="action" value="logout">
		<input type="hidden" name="<?php echo csrfTest(); ?>" value="<?php echo csrfToken(); ?>">
	</form>

	<div class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="<?php echo baseUrl().'administrator'; ?>">Administrator</a>
			</div>
			<div class="navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php htmlLang('Admin','article'); ?> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo baseUrl().'administrator/article/create'; ?>"><?php htmlLang('Admin','create_article'); ?></a></li>
							<li><a href="<?php echo baseUrl().'administrator/article'; ?>"><?php htmlLang('Admin','articles'); ?></a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php htmlLang('Admin','category'); ?> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo baseUrl().'administrator/category/create'; ?>"><?php htmlLang('Admin','create_category'); ?></a></li>
							<li><a href="<?php echo baseUrl().'administrator/category'; ?>"><?php htmlLang('Admin','categories'); ?></a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php htmlLang('Admin','profile'); ?> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo baseUrl().'administrator/user/profile'; ?>"><?php htmlLang('Admin','edit_profile'); ?></a></li>
							<li role="separator" class="divider"></li>
							<li><a href="#" onclick="$('#logout-frm').submit();"><?php htmlLang('Admin','logout'); ?></a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>