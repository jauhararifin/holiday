<!DOCTYPE html>
<html>
	<head>
		<title><?php htmlLang('Admin','login'); ?></title>

		<link rel="stylesheet" type="text/css" href="<?php echo $this->asset('css/bootstrap.min.css'); ?>">
		<script src="<?php echo $this->asset('js/bootstrap.min.js'); ?>"></script>
		<script src="<?php echo $this->asset('js/jquery.min.js'); ?>"></script>
	</head>
	<body>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<style>
			body {
				background-color: #f0f0f0;
				font-family: 'Open sans', sans-serif;
			}
			.window {
				background-color: #fff;
				box-shadow: 0 2px 4px rgba(0,0,0,0.2);
				margin:	100px auto;
				width: 400px;
			}
			.window .divider {
				height: 1px;
				overflow: hidden;
				background-color: #e5e5e5;
			}
			.window-header, .window-footer {
				padding: 20px 30px;
			}
			.window-content {
				padding: 40px 30px;
			}
			.window-left {
				text-align: left;
			}
			.window-center {
				text-align: center;
			}
			.window-right {
				float: right;
			}
			.textbox {
				margin-bottom: 20px;
				padding: 10px 20px;
				border: 1px solid #aaa;
				width: 100%;
			}
			.textbox input, .textbox input:focus {
				border: none;
				outline-color: transparent;
    			outline-style: none;
			}
			.textbox span {
				margin-left: -5px;
				margin-right: 5px;
			}
			.button {
				padding: 10px 30px;
				border: none;
			}
			.blue {
				background-color: #337ab7;
				color: #fff;
			}
			.button.blue:hover {
				background-color: #216EB1;
			}

		</style>

		<div class="window">
			<div class="window-header window-center blue">
				<h1><?php htmlLang('Admin','login'); ?></h1>
			</div>
			<div class="divider"></div>
			<form method="post" action="<?php echo baseUrl().'administrator/user/login/' ?>" class="window-content">
				<input type="hidden" name="action" value="login">
				<input type="hidden" name="<?php echo csrfTest(); ?>" value="<?php echo csrfToken(); ?>">
				<div class="textbox">
					<span class="glyphicon glyphicon-user"></span>
					<input name="username" type="text" placeholder="Username">
				</div>
				<div class="textbox">
					<span class="glyphicon glyphicon-lock"></span>
					<input name="password" type="password" placeholder="Password">
				</div>
				<div style="display:inline;" class="checkbox">
					<label style="margin-top: 10px;">
						<!-- <input name="remember" type="checkbox"> <?php htmlLang('Admin','remember_me'); ?> -->
					</label>
				</div>
				<button type="submit" class="button blue pull-right"><?php htmlLang('Admin','login'); ?></button>
			</form>
		</div>

		<script type="text/javascript" src="<?php echo $this->asset('js/noty/packaged/jquery.noty.packaged.min.js'); ?>"></script>
	    <script type="text/javascript">
	    $(document).ready(function() {
	        <?php foreach ($messageStatus->getMessages() as $type => $messages): ?>
	        <?php foreach ($messages as $mess): ?>
	        noty({
	          text  :"<?php html($mess); ?>",
	          type  :"<?php echo $type; ?>",
	          dismissQueue: true,
	          layout:'bottomRight',
	        });
	        <?php endforeach; ?>
	        <?php endforeach; ?>

	      });
	    </script>

	</body>
</html>
