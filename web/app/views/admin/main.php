<?php $this->render('header', array('title'=>configItem('siteName').' | Administrator')); ?>

	<div class="container">
		<div class="col-md-12">

			<div class="panel panel-default">
				<div class="panel-heading">
					Last 10 Articles
				</div>
				<table class="table">
					<tr>
						<th><?php htmlLang('Admin','id'); ?></th>
						<th><?php htmlLang('Admin','title'); ?></th>
						<th><?php htmlLang('Admin','author'); ?></th>
						<th><?php htmlLang('Admin','created'); ?></th>
						<th><?php htmlLang('Admin','updated'); ?></th>
						<th><?php htmlLang('Admin','status'); ?></th>
					</tr>
					<?php foreach ($articles as $article): ?>
					<tr>
						<td><?php html($article->id); ?></td>
						<td><?php html($article->title); ?></td>
						<td><?php html($article->author->fullname); ?></td>
						<td><?php html($article->time_created); ?></td>
						<td><?php html($article->time_updated); ?></td>
						<td><?php html(($article->status) ? lang('Admin','public') : lang('Admin','private')); ?></td>
					</tr>
					<?php endforeach; ?>
				</table>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">
					Categories
				</div>
				<table class="table">
					<tr>
						<th><?php htmlLang('Admin','id'); ?></th>
						<th><?php htmlLang('Admin','title'); ?></th>
						<th><?php htmlLang('Admin','author'); ?></th>
						<th><?php htmlLang('Admin','updated'); ?></th>
					</tr>
					<?php foreach ($categories as $category): ?>
					<tr>
						<td><?php html($category->id); ?></td>
						<td><?php html($category->title); ?></td>
						<td><?php html($category->author->fullname); ?></td>
						<td><?php html($category->time_updated); ?></td>
					</tr>
					<?php endforeach; ?>
				</table>
			</div>

		</div>
	</div>

<?php $this->render('footer'); ?>