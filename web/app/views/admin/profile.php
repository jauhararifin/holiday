<?php $this->render('header', array('title'=>configItem('siteName').' | Profile')); ?>

	<div class="container">
		<div class="col-md-12">

			<form class="form-horizontal" method="post" action="<?php echo baseUrl().'administrator/user/profile'; ?>">

				<input type="hidden" name="action" value="edit">
				<input type="hidden" name="<?php echo csrfTest(); ?>" value="<?php echo csrfToken(); ?>">
				
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php htmlLang('Admin','username'); ?></label>
					<div class="col-sm-10">
						<input name="username" type="text" class="form-control" value="<?php html($user->username); ?>" disabled>
					</div>
				</div>

				<?php
	                $fullnameErr = $editForm->validationError('fullname');
	                $fullnameStat = $editForm->attributeStatus('fullname');
	            ?>
				<div class="form-group <?php if ($fullnameStat == \InputValidation::SUCCESS) echo 'has-success'; else if ($fullnameStat == \InputValidation::VALIDATION_ERROR) echo 'has-error'; ?>">
					<label class="col-sm-2 control-label"><?php htmlLang('Admin','fullname'); ?></label>
					<div class="col-sm-10">
						<input name="fullname" type="text" class="form-control" value="<?php html($user->fullname); ?>">
						<?php if ($fullnameStat == \InputValidation::VALIDATION_ERROR): ?>
			            <span class="help-block"><?php html($fullnameErr[0]); ?></span>
			            <?php endif; ?>
					</div>
				</div>

				<?php
	                $nicknameErr = $editForm->validationError('nickname');
	                $nicknameStat = $editForm->attributeStatus('nickname');
	            ?>
				<div class="form-group <?php if ($nicknameStat == \InputValidation::SUCCESS) echo 'has-success'; else if ($nicknameStat == \InputValidation::VALIDATION_ERROR) echo 'has-error'; ?>">
					<label class="col-sm-2 control-label"><?php htmlLang('Admin','nickname'); ?></label>
					<div class="col-sm-10">
						<input name="nickname" type="text" class="form-control" value="<?php html($user->nickname); ?>">
						<?php if ($nicknameStat == \InputValidation::VALIDATION_ERROR): ?>
			            <span class="help-block"><?php html($nicknameErr[0]); ?></span>
			            <?php endif; ?>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label"><?php htmlLang('Admin','email'); ?></label>
					<div class="col-sm-10">
						<input name="email" type="email" class="form-control" value="<?php html($user->email); ?>" disabled>
					</div>
				</div>

				<?php
	                $date_birthErr = $editForm->validationError('date_birth');
	                $date_birthStat = $editForm->attributeStatus('date_birth');
	            ?>
				<div class="form-group <?php if ($date_birthStat == \InputValidation::SUCCESS) echo 'has-success'; else if ($date_birthStat == \InputValidation::VALIDATION_ERROR) echo 'has-error'; ?>">
					<label class="col-sm-2 control-label"><?php htmlLang('Admin','date_birth'); ?></label>
					<div class="col-sm-10">
						<input name="date_birth" type="text" class="form-control" value="<?php html($user->date_birth); ?>">
						<?php if ($date_birthStat == \InputValidation::VALIDATION_ERROR): ?>
			            <span class="help-block"><?php html($date_birthErr[0]); ?></span>
			            <?php endif; ?>
					</div>
				</div>

				<?php
	                $genderErr = $editForm->validationError('gender');
	                $genderStat = $editForm->attributeStatus('gender');
	            ?>
				<div class="form-group <?php if ($genderStat == \InputValidation::SUCCESS) echo 'has-success'; else if ($genderStat == \InputValidation::VALIDATION_ERROR) echo 'has-error'; ?>">
					<label class="col-sm-2 control-label"><?php htmlLang('Admin','gender'); ?></label>
					<div class="col-sm-10">
						<select name="gender" class="form-control">
							<option value="1" <?php if (($user->gender) == 1) echo 'selected'; ?> ><?php htmlLang('Admin','male'); ?></option>
							<option value="0" <?php if (($user->gender) != 1) echo 'selected'; ?> ><?php htmlLang('Admin','female'); ?></option>
						</select>
						<?php if ($genderStat == \InputValidation::VALIDATION_ERROR): ?>
			            <span class="help-block"><?php html($genderErr[0]); ?></span>
			            <?php endif; ?>
					</div>
				</div>

				<hr>

				<button type="submit" class="btn btn-primary pull-right"><?php htmlLang('Admin','edit_profile'); ?></button>

			</form>

		</div>
	</div>

<?php $this->render('footer'); ?>