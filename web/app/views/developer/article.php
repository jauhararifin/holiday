<?php $this->render('header', array('title'=>configItem('siteName').' | '.$article->title())); ?>
<div class="primary col-md-8 col-sm-12 col-xs-12">
    <section class="section">
        <div class="section-inner">
            <a href="<?php echo $article->url(); ?>"><h2 class="heading article"><?php html($article->title()); ?></h2></a>
            <div class="info article">
              <span><i class="fa fa-calendar"></i> <?php echo $article->time_updated('d F Y'); ?></span>
              <span><i class="fa fa-user"></i> <?php echo $article->author()->fullname(); ?></span>
              <span><i class="fa fa-comments"></i><?php $cn = $article->commentCount(); echo $cn; ?> Comment<?php if ($cn > 1) echo 's'; ?></span>
            </div>
            <div class="content">
              <?php echo $article->content(); ?>
            </div><!--//content-->
            <div class="bottom article">
              <?php foreach ($article->categories() as $category): ?>
              <a href="<?php echo $category->url(); ?>"><span class="label label-default"><?php echo $category->title(); ?></span></a>
              <?php endforeach; ?>
            </div>
        </div><!--//section-inner-->
    </section><!--//section-->

    <section class="comments section">
        <div class="section-inner">
            <?php if ($cn > 0): ?>
            <h2 class="heading">Comments</h2>

            <div class="content">

              <?php foreach ($comments as $comment): ?>
                <div class="item">
                    <h3 class="title"><?php echo $comment->fullname(); ?> - <span class="place"><?php echo $comment->time_created(); ?></span></h3>
                    <p><?php echo $comment->content(); ?></p>
                </div><!--//item-->
              <?php endforeach; ?>

            </div>
            <?php else: ?>
            <h2 class="heading" style="margin-bottom:0;">No comment</h2>
            <?php endif; ?>

            <nav>
              <ul class="pagination">
                <?php if ($pagination['pageNow'] > 1): ?>
                <li>
                  <a href="<?php echo $article->url()."?page=".($pagination['pageNow']-1); ?>" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                  </a>
                </li>
                <?php endif; ?>
                <?php for ($i = 1; $i <= $pagination['totalPage']; $i++): ?>
                <?php if ($i == $pagination['pageNow']): ?>
                <li class="active"><a href="#"><?php echo $i; ?></a></li>
                <?php else: ?>
                <li><a href="<?php echo $article->url()."?page=$i"; ?>"><?php echo $i; ?></a></li>
                <?php endif; ?>
                <?php endfor; ?>
                <?php if ($pagination['pageNow'] < $pagination['totalPage']): ?>
                <li>
                  <a href="<?php echo $article->url()."?page=".($pagination['pageNow']+1); ?>" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                  </a>
                </li>
                <?php endif; ?>
              </ul>
            </nav>

        </div><!--//section-inner-->
        <div class="divider"></div>

        <div class="section-inner">
          <h2 class="heading">Leave A Replay</h2>
          <div class="content">
            <form role="form" method="post" action="<?php echo $article->url(); ?>">
              <input type="hidden" name="action" value="comment">
              <input type="hidden" name="<?php echo csrfTest(); ?>" value="<?php echo csrfToken(); ?>">
              
              <?php
                $fullnameErr = $commentForm->validationError('fullname');
                $fullnameStat = $commentForm->attributeStatus('fullname');
              ?>
              <div class="form-group <?php if ($fullnameStat == \InputValidation::SUCCESS) echo 'has-success'; else if ($fullnameStat == \InputValidation::VALIDATION_ERROR) echo 'has-error'; ?>">
                <input name="fullname" type="text" class="form-control" placeholder="Full Name" value="<?php html ($commentForm->getData('fullname')); ?>">
                <?php if ($fullnameStat == \InputValidation::VALIDATION_ERROR): ?>
                <span class="help-block"><?php html($fullnameErr[0]); ?></span>
                <?php endif; ?>
              </div>

              <?php
                $emailErr = $commentForm->validationError('email');
                $emailStat = $commentForm->attributeStatus('email');
              ?>
              <div class="form-group <?php if ($emailStat == \InputValidation::SUCCESS) echo 'has-success'; else if ($emailStat == \InputValidation::VALIDATION_ERROR) echo 'has-error'; ?>">
                <input name="email" type="text" class="form-control" placeholder="Email" value="<?php html ($commentForm->getData('email')); ?>">
                <?php if ($emailStat == \InputValidation::VALIDATION_ERROR): ?>
                <span class="help-block"><?php html($emailErr[0]); ?></span>
                <?php endif; ?>
              </div>

              <?php
                $contentErr = $commentForm->validationError('content');
                $contentStat = $commentForm->attributeStatus('content');
              ?>
              <div class="form-group <?php if ($contentStat == \InputValidation::SUCCESS) echo 'has-success'; else if ($contentStat == \InputValidation::VALIDATION_ERROR) echo 'has-error'; ?>">
                <textarea name="content" class="form-control" rows="5" placeholder="Comment"><?php html ($commentForm->getData('content')); ?></textarea>
                <?php if ($contentStat == \InputValidation::VALIDATION_ERROR): ?>
                <span class="help-block"><?php html($contentErr[0]); ?></span>
                <?php endif; ?>
              </div>
              
              <button type="submit" class="btn btn-default">Send Comment</button>
            </form>
          </div>
        </div><!--//section-inner-->

    </section><!--//section-->

</div><!--//primary-->
<div class="secondary col-md-4 col-sm-12 col-xs-12">
  <?php $this->render('sidebar'); ?>
</div><!--//secondary-->
<?php $this->render('footer'); ?>
