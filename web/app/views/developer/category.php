<?php $this->render('header', array('title'=>configItem('siteName').' | '.$category->title())); ?>
<div class="primary col-md-8 col-sm-12 col-xs-12">
    <?php if (count($articles) > 0): ?>
    <?php foreach ($articles as $article): ?>
    <section class="section">
        <div class="section-inner">
            <a href="<?php echo $article->url(); ?>"><h2 class="heading article"><?php html($article->title()); ?></h2></a>
            <div class="info article">
              <span><i class="fa fa-calendar"></i> <?php echo $article->time_updated(); ?></span>
              <span><i class="fa fa-user"></i> <?php echo $article->author()->fullname(); ?></span>
              <span><i class="fa fa-comments"></i> <?php $cn = $article->commentCount(); echo $cn; ?> Comment<?php if ($cn > 1) echo 's'; ?></span>
            </div>
            <div class="content">
              <?php echo wordLimit($article->content(), 75,'<a href="'.$article->url().'">... [More]</a>'); ?>
            </div><!--//content-->
            <div class="bottom article">
              <?php foreach ($article->categories() as $category): ?>
              <a href="<?php echo $category->url(); ?>"><span class="label label-default"><?php echo $category->title(); ?></span></a>
              <?php endforeach; ?>
            </div>
        </div><!--//section-inner-->
    </section><!--//section-->
    <?php endforeach; ?>
    <?php else: ?>
      <section class="section">
        <div class="section-inner">
          <h1>No Article</h1>
        </div>
      </section>
    <?php endif; ?>

    <nav>
      <ul class="pagination">
        <?php if ($pagination['pageNow'] > 1): ?>
        <li>
          <a href="<?php echo $category->url()."?page=".($pagination['pageNow']-1); ?>" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
          </a>
        </li>
        <?php endif; ?>
        <?php for ($i = 1; $i <= $pagination['totalPage']; $i++): ?>
        <?php if ($i == $pagination['pageNow']): ?>
        <li class="active"><a href="#"><?php echo $i; ?></a></li>
        <?php else: ?>
        <li><a href="<?php echo $category->url()."?page=$i"; ?>"><?php echo $i; ?></a></li>
        <?php endif; ?>
        <?php endfor; ?>
        <?php if ($pagination['pageNow'] < $pagination['totalPage']): ?>
        <li>
          <a href="<?php echo $category->url()."?page=".($pagination['pageNow']+1); ?>" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
          </a>
        </li>
        <?php endif; ?>
      </ul>
    </nav>

</div><!--//primary-->
<div class="secondary col-md-4 col-sm-12 col-xs-12">
  <?php $this->render('sidebar'); ?>
</div><!--//secondary-->
<?php $this->render('footer'); ?>
