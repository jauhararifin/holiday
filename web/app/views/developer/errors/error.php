<?php $this->render('header', array('title'=>$title)); ?>

<section class="section" style="width: 70%; text-align:center; margin: auto;">
    <div class="section-inner">
        <h2 class="heading article"><?php html($title); ?> (<?php echo $statusCode; ?>)</h2>
        <div class="content">
        	<?php echo $message; ?>
        </div><!--//content-->
    </div><!--//section-inner-->
</section><!--//section-->