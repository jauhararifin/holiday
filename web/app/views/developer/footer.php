      </div><!--//row-->
    </div><!--//masonry-->

    <!-- ******FOOTER****** -->
    <footer class="footer">
      <div class="container text-center">
        <small class="copyright">Designed with <i class="fa fa-heart"></i> by <a href="http://themes.3rdwavemedia.com" target="_blank">Xiaoying Riley</a> for developers</small>
      </div><!--//container-->
    </footer><!--//footer-->

    <!-- Javascript -->
    <script type="text/javascript" src="<?php echo $this->asset('plugins/jquery-1.11.2.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $this->asset('plugins/jquery-migrate-1.2.1.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $this->asset('plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $this->asset('plugins/jquery-rss/dist/jquery.rss.min.js'); ?>"></script>
    <!-- github activity plugin -->
    <script type="text/javascript" src="<?php echo $this->asset('plugins/github-activity/dist/mustache/mustache.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $this->asset('plugins/github-activity/dist/github-activity-0.1.1.min.js'); ?>"></script>
    <!-- custom js -->
    <script type="text/javascript" src="<?php echo $this->asset('js/main.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $this->asset('js/jquery-2.1.4.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $this->asset('js/noty/packaged/jquery.noty.packaged.min.js'); ?>"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        <?php foreach ($messageStatus->getMessages() as $type => $messages): ?>
        <?php foreach ($messages as $mess): ?>
        noty({
          text  :"<?php html($mess); ?>",
          type  :"<?php echo $type; ?>",
          dismissQueue: true,
          layout:'bottomRight',
        });
        <?php endforeach; ?>
        <?php endforeach; ?>

      });
    </script>
  </body>
</html>
