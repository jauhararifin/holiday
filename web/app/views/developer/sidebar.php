<aside class="info aside section">
   <div class="section-inner" style="text-align:center;">
          <a href="<?php echo baseUrl(); ?>"><h3 style="margin:0;"><?php html(configItem('siteName')); ?></h3></a>
   </div><!--//section-inner-->
</aside><!--//aside-->

<aside class="blog aside section">
   <div class="section-inner">
       <h2 class="heading">Latest Blog Posts</h2>
       <p>You can use Sascha Depold's <a href="https://github.com/sdepold/jquery-rss" target="_blank">jQuery RSS plugin</a> to pull in your blog post feeds.</p>
       <div id="rss-feeds" class="content">

       </div><!--//content-->
   </div><!--//section-inner-->
</aside><!--//section-->
