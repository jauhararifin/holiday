<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="<?php echo $this->asset('css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo $this->asset('css/style.css'); ?>">
    <title><?php echo \Holiday::configItem('siteName'); ?></title>
  </head>
  <body>
    <div class="container-fluid body-wrapper">
      <div class="row">
        <div class="col-md-8">
          <div class="post-box">
            <article>
              <header>
                <h1 class="title"><?php echo $article->title; ?></h1>
              </header>
              <address>
                <span class="info">
                  <span class="glyphicon glyphicon-user"></span>
                  <span><?php echo $article->author->fullname; ?></span>
                </span>
                <span class="info">
                  <span class="glyphicon glyphicon-calendar"></span>
                  <span><?php echo $article->time_updated; ?></span>
                </span>
              </address>
              <div class="content">
                <?php echo $article->content; ?>
              </div>
            </article>
          </div>
        </div>
        <div class="col-md-4">
        </div>
      </div>
    </div>
  </body>
</html>
