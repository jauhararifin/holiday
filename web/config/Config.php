<?php

return [

	'baseUrl'	=> 'http://localhost/holiday/',
	'siteName' 	=> 'Holiday',

	'importFromDatabase'	=> TRUE,
	'configTable'			=> 'config',

	'database'	=> require_once('Database.php'),

	'security'	=> require_once('Security.php'),

	'route'		=> require_once('Route.php'),

	'view'		=> require_once('View.php'),

	'language'	=> require_once('Language.php'),

	'autoload_helpers'	=> ['config','security','html','language','url'],

];
