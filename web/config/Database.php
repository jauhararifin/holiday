<?php

return array(

		'active' => 'default',

		'default' => array(

				'driver'		=> 'mysql',
				'host'			=> 'localhost',
				'database'		=> 'holiday',
				'username'		=> 'root',
				'password'		=> '',
				'charset'		=> 'utf8',
				'collation'		=> 'utf8_unicode_ci',
				'prefix'		=> 'hl_',
				'options'		=> array(),

			),

	);
