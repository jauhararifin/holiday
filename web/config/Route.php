<?php

return [

	'defaultRoute'	=> 'Category',
	'rewrite' =>  [
		'/administrator\/?$/'	=> 'administrator/main',
		'/administrator\/(.+)/'	=> 'administrator/$1',
		'/article\/(.+)/'		=> 'article/read/$1',
		'/category\/(.+)/'		=> 'category/show/$1',
	],

];
