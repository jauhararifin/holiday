<?php

namespace core;

class Application {

	private $_instance = NULL;

	public $config = NULL;
	public $route = NULL;
	public $session = NULL;
	public $security = NULL;
	public $controller = NULL;
	public $language = NULL;

	public function __construct(string $configFile)
	{
		$this->config = new Config($configFile);
		$this->session = new Session($this->config->item('session') ?? []);
		$this->language = new Language($this->config->item('language') ?? []);

		\helpers\Loader::load('language');
		
		foreach ($this->config->item('autoload_helpers') as $helper)
			\helpers\Loader::load($helper);
	}

	public function run()
	{
		if ($this->config->item('importFromDatabase'))
			$this->config->loadDatabase($this->config->item('configTable'));

		$this->language->loadLanguage('Core');

		$this->security = new Security($this->config->item('security'));
		$this->security->run();

		$this->route = new Route($this->config->item('route'));

		if (! $this->route->parseRequest())
			return;

		$vars = array();
		foreach (array('directory', 'file', 'class', 'action', 'params') as $key)
			$vars[$key] = $this->route->$key;
		extract($vars);

		$action_func = Route::ACTION_PREFIX.$action.Route::ACTION_SUFFIX;

		$this->controller = new $class();
		if ($this->controller === NULL)
		{
			\Holiday::showError(lang('Core','unable_create_controller'));
			return;
		}

		$remap_func = \core\Route::ACTION_REMAP_METHOD;

		if (method_exists($this->controller, $remap_func))
			$this->controller->$remap_func($action, $params);
		else if (method_exists($this->controller, $action_func))
			call_user_func_array(array($this->controller, $action_func), $params);
		else
			\Holiday::showError(lang('Core','unable_find_action'));
	}

}
