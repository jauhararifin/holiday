<?php

namespace core;

class Config {

	private $_config = array();

	public function __construct(string $configFile='')
	{
		$this->loadFile($configFile);
	}

	public function loadFile(string $configFile)
	{
		if (! file_exists($configFile))
			\Holiday::showError(lang('Core','file_not_exists',array('file'=>$configFile)));
		else
		{
			$config = require ($configFile);
			if (! is_array($config))
				return;
			foreach ($config as $key => $value)
				$this->_config[$key] = $value;
		}
	}

	public function loadDatabase(string $table)
	{
		$db = new \core\Model;
		$db = $db->db((string) $table);
		$query = $db->get();

		foreach ($query as $row)
		{
			switch (strtolower($row->type))
			{
				case 'json':
					$value = json_decode($row->value);
					break;
				case 'int':
					$value = intval($row->value);
					break;
				default:
					$value = (string) $row->value;
					break;
			}
			$this->setItem($row->key, $value);
		}
	}

	public function item(string $key)
	{
		$path = explode ('.', $key);
		$var = & $this->_config;
		for ($i = 0; $i < count($path)-1; $i++)
		{
			$p = $path[$i];
			if (! isset($var[$p]) || ! is_array($var[$p]) )
				return NULL;
			$var = & $var[$p];
		}
		if (! isset($var[$path[count($path)-1]]))
			return NULL;
		return $var[$path[count($path)-1]];
	}

	public function setItem(string $key, $value)
	{
		$path = explode ('.', $key);
		$var = & $this->_config;
		foreach ($path as $p)
		{
			if (! isset($var[$p]) || ! is_array($var[$p]) )
				$var[$p] = array();
			$var = & $var[$p];
		}
		$var = $value;
	}

}
