<?php

namespace core;

class Database {

	private $_config;

	private $_pixie;

	public function __construct(array $config)
	{
		$this->_config = $config;

		$connection = new \Pixie\Connection($config['driver'], $config);
		$this->_pixie = new \Pixie\QueryBuilder\QueryBuilderHandler($connection);
	}

	public function px(string $tableName) : \Pixie\QueryBuilder\QueryBuilderHandler
	{
		return $this->_pixie->table($tableName);
	}

}
