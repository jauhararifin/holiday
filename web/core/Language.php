<?php

namespace core;

class Language {

	private $_current_lang = 'english';

	private $_language = [];

	public function __construct(array $config = [])
	{
		$this->_language = [];
		$this->setLanguage($config['default_language'] ?? 'english');
	}

	public function loadLanguage(string $file, string $lang = '')
	{
		if ($lang == '')
			$lang = $this->_current_lang;

		$location = [SYSTEM_PATH.'app/language/', SYSTEM_PATH.'language/'];

		foreach ($location as $loc)
		{
			$path = $loc.$lang.'/'.$file.'.php';
			if (file_exists($path))
			{
				if (!is_array($this->_language))
					$this->_language = [];
				if (!isset($this->_language[$lang]))
					$this->_language[$lang] = [];

				if (!isset($this->_language[$lang][$file]) || !is_array($this->_language[$lang][$file]))
					$this->_language[$lang][$file] = [];

				$data = include_once ($path);
				if (is_array($data))
					foreach ($data as $key => $val)
						$this->_language[$lang][$file][$key] = $val;

				break;
			}
		}
	}

	public function setLanguage(string $lang)
	{
		$this->_current_lang = $lang;
	}

	public function language(string $file, string $key = '', array $params = [], string $lang = '') : string
	{
		if ($lang == '')
			$lang = $this->_current_lang;

		if (!is_array($this->_language))
			return "$file.$key";
		if (!isset($this->_language[$lang]) || !is_array($this->_language[$lang]))
			return "$file.$key";

		$str = '';

		if ($key == '')
		{
			foreach ($this->_language as $f => $list)
				if (is_array($list))
					foreach ($list as $k => $v)
						if ($k === $file)
							$str = (string) $v;
		} else
		{
			if (!isset($this->_language[$lang][$file]) || !is_array($this->_language[$lang][$file]))
				return "$file.$key";
			if (!isset($this->_language[$lang][$file][$key]))
				return "$file.$key";
			$str = (string) $this->_language[$lang][$file][$key];
		}

		$search = [];
		$replace = [];
		if (is_array($params))
			foreach ($params as $p => $q) {
				$search[] = '{{'.$p.'}}';
				$replace[] = $q;
			}
		str_replace($search, $replace, $str);

		return $str;
	}

}
