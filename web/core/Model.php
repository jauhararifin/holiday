<?php

namespace core;

class Model {

	private static $_db;

	public function __construct() {}

	public static function db(string $tblName='') : \Pixie\QueryBuilder\QueryBuilderHandler
	{
		if (self::$_db === NULL)
		{
			$config = \Holiday::$app->config->item('database');

			$active_db = isset($config['active']) ? $config['active'] : 'default';
			if (! array_key_exists($active_db, $config))
				exit(lang('Core','unable_find_database_configuration'));

			self::$_db = new Database($config[$active_db]);
		}
		return self::$_db->px($tblName);
	}

}
