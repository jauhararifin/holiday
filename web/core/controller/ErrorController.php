<?php

namespace core\controller;

class ErrorController extends \core\Controller {

	protected $message		= '';
	protected $statusCode	= 500;
	protected $template		= 'error';
	protected $title		= 'Something error.';
	protected $return		= FALSE;

	public function __construct(string $message, int $statusCode, string $template, string $title, bool $return)
	{
		$this->message		= $message;
		$this->statusCode	= $statusCode;
		$this->template		= $template;
		$this->title		= $title;
		$this->return		= $return;

		\Holiday::setHeaderStatus ($statusCode);
		while (ob_get_level() > 1)
			ob_end_flush();
	}

	public function actionShow()
	{
		$message	= $this->message;
		$statusCode = $this->statusCode;
		$template 	= $this->template;
		$title 		= $this->title;
		$return 	= $this->return;

		ob_start();
		include SYSTEM_PATH.'errors/error.php';
		$output = ob_get_contents();
		ob_end_clean();

		if ($return)
			return $output;
		else
			echo $output;
	}

}
