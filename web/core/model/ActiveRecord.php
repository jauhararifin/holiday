<?php

namespace core\model;

class ActiveRecord extends \core\Model {

	const HAS_ONE		= 0;
	const MANY_MANY		= 1;
	const HAS_MANY		= 3;

	private $_attribute = [];

	public function __construct()
	{
		parent::__construct();

		$class = get_called_class();
	}

	public function __set(string $key, $val)
	{
		$class = get_called_class();
		if (array_key_exists($key, $class::$columns))
			$this->_attribute[$key] = $val;
	}

	public function __get(string $key)
	{
		$class = get_called_class();
		if (is_array($class::$columns) && isset($class::$columns[$key]))
			return $this->_attribute[$key] ?? NULL;

		if (is_array($class::$relations) && isset($class::$relations[$key]))
			return $this->_relate($class::$relations[$key]);

		return $this->$key ?? NULL;
	}

	public function __call(string $method, array $args)
	{
		$class = get_called_class();

		if (is_array($class::$columns) && isset($class::$columns[$method]))
			return $this->_attribute[$method] ?? NULL;

		if (isset($class::$relations[$method]))
		{
			$relation = $class::$relations[$method];

			if (is_array($args) && isset($args[0]))
				foreach ($args[0] as $key => $value)
					$relation[$key] = $value;

			return $this->_relate($relation);
		}

		return call_user_func_array(array($this, $method), $args);
	}

	public function toArray(array $data = [])
	{
		$class = get_called_class();

		if (empty($data))
		{
			$record = [];
			foreach ($class::$columns as $key => $val)
				$record[$key] = $this->_attribute[$key] ?? NULL;
			return $record;
		}

		$res = [];
		foreach ($data as $dat)
			$res[] = $dat->toArray();
		return $res;
	}

	public function count(string $param = '', array $args=[]) : int
	{
		$class = get_called_class();

		if ($param == '')
		{
			$criteria = $args['criteria'] ?? [];
			$limit = $args['limit'] ?? -1;
			$offset = $args['offset'] ?? 0;
			$orderBy = $args['orderBy'] ?? [];

			return intval($class::findByCriteria($criteria, $limit, $offset, $orderBy, FALSE)->count());
		}

		if (isset($class::$columns[$param]))
			return 1;

		if (! isset($class::$relations[$param]['type']))
			return 0;

		$type = $class::$relations[$param]['type'];
		if ($type == self::HAS_MANY || $type == self::MANY_MANY)
		{
			$relation = $class::$relations[$param];
			if (isset($args))
				foreach ($args as $key => $value)
					$relation[$key] = $value;
			
			$res = $this->_relate($relation, FALSE);
			return intval($res->count());
		}
		return 1;
	}

	private function _relate(array $relation, bool $getQuery = TRUE)
	{
		if (! array_key_exists('type', $relation))
			return NULL;

		extract($relation);

		switch ($type)
		{
			case self::HAS_ONE:
				if (!isset($foreignClass,$domesticKey,$foreignKey))
					return NULL;
				$foreignClass = new $foreignClass;
				$cri = [['where',$foreignKey,'=',$this->$domesticKey]];
				if (isset($criteria) && is_array($criteria))
					$cri = array_merge($criteria, $cri);

				$res = $foreignClass::findUnique($cri,[],$getQuery);
				return $res;

			case self::HAS_MANY:
				if (!isset($foreignClass,$domesticKey,$foreignKey))
					return NULL;

				$foreignClass = new $foreignClass;

				$limit = isset($limit) ? intval($limit) : -1;
				$offset = isset($offset) ? intval($offset) : 0;
				$orderBy = isset($orderBy) ? $orderBy : [];
				$criteria = isset($criteria) ? $criteria : [];

				$fbc = array(array('where',$foreignKey,'=',$this->$domesticKey));
				foreach ($criteria as $c)
					$fbc[] = $c;

				$res = $foreignClass::findByCriteria($fbc, $limit, $offset, $orderBy, $getQuery);

				return $res;

			case self::MANY_MANY:
				if (!isset($foreignClass,$domesticKey,$foreignKey,$connectionTable))
					return NULL;

				$foreignTable = $foreignClass::$tableName;

				$limit = isset($limit) ? intval($limit) : -1;
				$offset = isset($offset) ? intval($offset) : 0;
				$orderBy = isset($orderBy) ? $orderBy : NULL;
				$criteria = isset($criteria) ? $criteria : [];

				$query = $this->db($foreignTable)->select($foreignTable.'.*');
				$query->join($connectionTable, $connectionTable.'.'.$foreignKey[0], '=', $foreignTable.'.'.$foreignKey[1], 'INNER');

				$query->where($connectionTable.'.'.$domesticKey[1],'=',$this->{$domesticKey[0]});

				foreach ($criteria as $wh)
					if (is_array ($wh) && count($wh) > 0)
						call_user_func_array(array($query,(string) $wh[0]), array_slice($wh,1));

				if ($limit >= 0)
					$query->limit($limit)->offset($offset);

				if (is_array($orderBy) && isset($orderBy['field']))
					$query->orderBy($orderBy['field'], isset($orderBy['order']) ? $orderBy['order'] : 'ASC');

				if ($getQuery)
					$query = $query->get();
				else
					return $query;

				$arr = [];
				foreach ($query as $row)
				{
					$obj = new $foreignClass;
					foreach ($obj::$columns as $key => $dumb)
						$obj->$key = $row->$key;
					$arr[] = $obj;
				}
				return $arr;
		}
		return NULL;
	}

	/*
		Contoh criteria :
		array (
			array(where, field, op, val)
			array(whereNot, 'field', '=', 'value')
		);
	*/
	public static function findByCriteria(array $criteria, int $limit=-1, int $offset=0, array $orderBy = [], bool $getQuery = TRUE)
	{
		$class = get_called_class();

		$query = self::db($class::$tableName);
		foreach ($criteria as $wh)
			if (is_array ($wh) && count($wh) > 0)
				call_user_func_array(array($query,(string) $wh[0]), array_slice($wh,1));

		if ($limit >= 0)
			$query->limit($limit)->offset($offset);

		if (isset($orderBy['field']) || isset($orderBy[0]))
			$query->orderBy($orderBy['field'] ?? $orderBy[0], $orderBy['order'] ?? $orderBy[1] ?? 'ASC');

		if ($getQuery)
			$query = $query->get();
		else
			return $query;

		$columns = $class::$columns;
		$arr = [];
		foreach ($query as $row)
		{
			$className = $class::$className;
			$obj = new $className;
			foreach ($columns as $key => $dumb)
				$obj->$key = $row->$key;
			$arr[] = $obj;
		}

		return $arr;
	}

	public static function findUnique(array $criteria, array $orderBy = [], $getQuery = TRUE)
	{
		$result = self::findByCriteria($criteria, 1, 0, $orderBy, $getQuery);

		if (count($result) > 0)
			return $result[0];

		return NULL;
	}

	public function insert($datas = NULL, array $field = [])
	{
		$class = get_called_class();

		if ($datas !== NULL && is_array($datas))
		{
			$in = [];
			foreach ($datas as $data)
			{
				$item = [];
				foreach ($data->_attribute as $key => $val)
					if (empty($field) || in_array($key, $field))
						$item[$key] = $val;
				$in[] = $item;
			}
			return self::db($class::$tableName)->insert($in);
		}

		$item = [];
		foreach ($this->_attribute as $key => $val)
			if (empty($field) || in_array($key, $field))
				$item[$key] = $val;

		try {
			return self::db($class::$tableName)->insert($item);
		} catch(PDOException $e) {
			return 0;
		}
	}

	public function update(string $key = '', array $field = [])
	{
		$class = get_called_class();

		if ($key == '')
			$key = $class::$primaryKey ?? '';
		if ($key == '')
			return 0;
		
		$data = [];
		foreach ($class::$columns as $i => $x)
			if (empty($field) || in_array($i, $field))
				$data[$i] = $this->$i ?? NULL;
		
		return self::db($class::$tableName)->where($key,$this->$key)->update($data);
	}

	public function delete(string $key = '')
	{
		$class = get_called_class();

		if ($key == '')
			$key = $class::$primaryKey ?? '';
		if ($key == '')
			return 0;

		return self::db($class::$tableName)->where($key,$this->$key)->delete();
	}

	public static $className = '\\core\\model\\ActiveRecord';
	public static $tableName = '';
	public static $relations = [];
	public static $columns	  = [];

}
