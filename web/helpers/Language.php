<?php

function lang(string $file, string $key = '', array $params = [], string $lang = '') {
	return \Holiday::$app->language->language($file, $key, $params, $lang);
}

function line(string $file, string $key = '', array $params = [], string $lang = '') {
	echo \Holiday::$app->language->lang($file, $key, $params, $lang);
}

function htmlLang(string $file, string $key = '', array $params = [], string $lang = '') {
	echo htmlentities(lang($file, $key, $params, $lang));
}