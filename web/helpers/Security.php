<?php

function csrfTest() : string
{
  return \Holiday::$app->security->getCsrfTestName();
}

function csrfToken() : string
{
  return \Holiday::$app->security->getCsrfToken();
}

function csrfVerify(string $from, bool $error = FALSE) : bool
{
	return \Holiday::$app->security->csrfVerify($from, $error);
}
