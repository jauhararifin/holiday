<?php

return array(

	'unable_find_controller'	=> "Unable to find specified controller.",
	'unable_create_controller'	=> "Unable to create controller class.",
	'unable_find_action'		=> "Unable to find specified action.",
	'file_not_exists'			=> "File not exists {{file}}.",
	'unable_find_database_configuration'	=> "Unable to find database configuration.",
	'action_not_permitted'		=> "Action not permitted.",
	'something_error'			=> "Something error.",
	'system_under_construction_internal_error'	=> "System is under construction. Internal server error.",

);